package engine.renderer.forwardjogl.object;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import engine.renderer.GenericBuffer;
import geometry.common.MessageOutput;


public class GLBuffer extends GenericBuffer<GL2>{

	private static final int BYTES_PER_SHORT = 2;
	private static final int BYTES_PER_FLOAT = 4;
	
	private int bufferID;
	private int floatsPerAttribute; //only used if created as vertex attribute buffer
	
	//constructor
	public static class Constructor implements BufferConstructor<GL2, GLBuffer>{
		
		@Override
		public GLBuffer construct(GL2 context, float[] fArray, int floatsPerAttribute){
			return new GLBuffer(context, fArray, floatsPerAttribute);
		}
		
		@Override
		public GLBuffer construct(GL2 context, int[] iArray){
			return new GLBuffer(context, iArray);
		}
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; }
	
	
	protected GLBuffer(GL2ES2 gl, float[] fArray, int floatsPerAttribute){
	
		this.floatsPerAttribute = floatsPerAttribute;
		bufferID = createAndFillVertexAttribBuffer(gl, fArray);
	}
	
	protected GLBuffer(GL2ES2 gl, int[] iArray){
		
		bufferID = createAndFillElementArrayBuffer(gl, iArray);
	}
	
	private static int createAndFillVertexAttribBuffer(GL2ES2 gl, float[] fArray){
		
		int[] hArray = new int[1];
		gl.glGenBuffers(1, hArray, 0);
		int h = hArray[0];
		
		gl.glBindBuffer(GL2ES2.GL_ARRAY_BUFFER, h);

		FloatBuffer data = FloatBuffer.allocate(fArray.length); //BufferUtil.newFloatBuffer(fArray.length);
		for(int i=0; i<fArray.length; i++){ data.put(i,fArray[i]); }
		//TODO: why doesnt data.put(fArray) work?
		gl.glBufferData(GL2ES2.GL_ARRAY_BUFFER, fArray.length*BYTES_PER_FLOAT, data, GL2ES2.GL_STATIC_DRAW);
		
		return h;
	}
	
	private static int createAndFillElementArrayBuffer(GL2ES2 gl, int[] iArray){
		
		int[] hArray = new int[1];
		gl.glGenBuffers(1, hArray, 0);
		int h = hArray[0];
		
		gl.glBindBuffer(GL2ES2.GL_ELEMENT_ARRAY_BUFFER, h);

		ShortBuffer data = ShortBuffer.allocate(iArray.length);
		for(int i=0; i<iArray.length; i++){ data.put(i,(short)iArray[i]); }
		//TODO: why doesnt data.put(fArray) work?
		gl.glBufferData(GL2ES2.GL_ELEMENT_ARRAY_BUFFER, iArray.length*BYTES_PER_SHORT, data, GL2ES2.GL_STATIC_DRAW);
		
		return h;
	}
	
	//[TODO] buffer modification
	//public void modify(GL2 gl, long offset, long size, Buffer data){
	//	
	//	gl.glBufferSubData(bufferID, offset, size, data);
	//}
	
	@Override
	public void delete(GL2 gl){
		
		int[] hArray = new int[]{ bufferID };
		gl.glDeleteBuffers(hArray.length, hArray, 0);
		
		MessageOutput.printDebug("Deleted array buffer");
	}
	
	//bind
	public void bindAsElementArray(GL2ES2 gl){
		
		gl.glBindBuffer(GL2ES2.GL_ELEMENT_ARRAY_BUFFER, bufferID);
	}
	
	public void bindAsVertexAttrib(GL2ES2 gl, int vertexAttributeIndex){ 
		
		gl.glBindBuffer(GL2ES2.GL_ARRAY_BUFFER, bufferID);
		gl.glVertexAttribPointer(vertexAttributeIndex, floatsPerAttribute, GL2ES2.GL_FLOAT, false, 0, 0);
	}
}
