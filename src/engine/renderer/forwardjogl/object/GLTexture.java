package engine.renderer.forwardjogl.object;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GLProfile;

import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;

import engine.renderer.GenericTexture;
import engine.renderer.forwardjogl.buffer.BufferColor;
import engine.renderer.forwardjogl.shader.quad.ShaderQuad;
import engine.tools.Quad;
import engine.toolsAWT.BufferTexture;
import geometry.common.MessageOutput;
import geometry.math.Vector4d;


public class GLTexture extends GenericTexture<GL2>{

	private int width, height;
	private int texID;
	
	
	//constructor
	public static class Constructor{
		public GLTexture construct(GL2 context, 
			InputStream inputStream, String format, boolean generateMipmaps, boolean wrap){
			
			return new GLTexture(context, inputStream, format, generateMipmaps, wrap);
		}
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; } 
	
	
	public GLTexture(int texID, int width, int height){
		
		this.texID = texID;
		this.width = width;
		this.height = height;
	}

	
	public GLTexture(GL2ES2 gl, InputStream inputStream, String format, 
		boolean generateMipmaps, boolean wrap){

		//generate textures
		int[] createTexIDs = new int[1];
		gl.glGenTextures(createTexIDs.length, createTexIDs, 0);
		texID = createTexIDs[0];    
		
		//populate the texture data
		//TODO
		if(format == "tga"){
			initializeTexture(gl, inputStream, format, texID, generateMipmaps, wrap);
		}else{
			initializeTexture(gl, inputStream, format, texID, generateMipmaps, wrap);
		}
	}
	
	@Override
	public void delete(GL2 gl){
		
		int[] hArray = new int[]{ texID };
		gl.glDeleteTextures(hArray.length, hArray, 0);
		
		MessageOutput.printDebug("Deleted texture");
	}
	
	private void initializeTexture(GL2ES2 gl, InputStream inputStream, String format, int texID, 
		boolean generateMipmaps, boolean wrap){
		
		MessageOutput.printDebug("Initializing texture ...");
		
		TextureData data = null;
		try{ 
			data = TextureIO.newTextureData(GLProfile.getDefault(), inputStream, false, format);
			inputStream.close();
			
			MessageOutput.printDebug("Loaded texture, estimated size " + (int)((float)data.getEstimatedMemorySize()/1024.0f) + " KB");
		}
		catch(IOException e){ e.printStackTrace(); }
		
		//store width and height
		this.width = data.getWidth();
		this.height = data.getHeight();
		
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, texID);
		
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_WRAP_S, wrap ? GL2ES2.GL_REPEAT : GL2ES2.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_WRAP_T, wrap ? GL2ES2.GL_REPEAT : GL2ES2.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_MAG_FILTER, GL2ES2.GL_LINEAR);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_MIN_FILTER, 
			generateMipmaps ? GL2ES2.GL_LINEAR_MIPMAP_LINEAR : GL2ES2.GL_LINEAR);
		
		gl.glTexImage2D(GL2ES2.GL_TEXTURE_2D, 0, 
				data.getInternalFormat(), 
				data.getWidth(), data.getHeight(), 0, 
	    		data.getPixelFormat(), data.getPixelType(),
	    		data.getBuffer());
		if(generateMipmaps){ gl.glGenerateMipmap(GL2ES2.GL_TEXTURE_2D); }
		
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, 0);
	}
		
	@Override
	public int getWidth(){ return width; }
	
	@Override
	public int getHeight(){ return height; }
	
	
	public void bind(GL2ES2 gl, int texUnitColor){

		gl.glActiveTexture(GL2ES2.GL_TEXTURE0+texUnitColor);
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, texID);
	}
	
	public void unbind(GL2ES2 gl, int texUnitColor){
		
		gl.glActiveTexture(GL2ES2.GL_TEXTURE0+texUnitColor);
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, 0);
	}

	public void bindReadFrom(GL2 gl){
		
		//attach texture
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, 
			GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, texID, 0);
		
		//specify drawbuffers and readbuffers
		//TODO: not necessary on GLES2?
		gl.glDrawBuffer(GL2.GL_NONE);
		gl.glReadBuffer(GL2.GL_COLOR_ATTACHMENT0);
		
		//check if it worked
		int status = gl.glCheckFramebufferStatus(GL2.GL_FRAMEBUFFER);
		if(status != GL2.GL_FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
		
		//set appropriate viewport
		gl.glViewport(0, 0, width, height);
	}
	
	public void unbindReadFrom(GL2ES2 gl){
		
		//detach texture
		gl.glFramebufferTexture2D(GL2ES2.GL_FRAMEBUFFER, 
			GL2ES2.GL_COLOR_ATTACHMENT0, GL2ES2.GL_TEXTURE_2D, 0, 0);
	}
	
	/**
	 * Uses glReadPixels, which is slow.  Only use this if it is really necessary.
	 */
	@Override
	public void getAsBufferTexture(GL2 gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight){
		
		//[TODO] can do e.g. gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1) instead, 
		//but maybe there is a reason not to; is it slower?
		srcWidth += (4 - (srcWidth % 4)) % 4;
		srcHeight += (4 - (srcHeight % 4)) % 4;
		
		//get buffer
		ByteBuffer data = target.getBufferForWriting(srcWidth, srcHeight);
		
		//read in the rect of pixel data
		bindReadFrom(gl);
		gl.glReadPixels(srcMinX, srcMinY, srcWidth, srcHeight, 
			GL2.GL_BGRA, GL2.GL_UNSIGNED_BYTE, data);
		unbindReadFrom(gl);
	}
	
	/**
	 * Uses glReadPixels, which is slow.  Only use this if it is really necessary.
	 */
	@Override
	public void getAsBufferTexture(GL2 gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight,
		int targetScaledWidth, int targetScaledHeight){
		
		//find appropriate scaling
		float sx = (srcWidth <= targetScaledWidth) ? 1.0f : (float)targetScaledWidth/(float)srcWidth;
		float sy = (srcHeight <= targetScaledHeight) ? 1.0f : (float)targetScaledHeight/(float)srcHeight;
		float s = (sx < sy) ? sx : sy;
		
		int scaledWidth = (int)(srcWidth * s);
		int scaledHeight = (int)(srcHeight * s);
		
		//ensure target size has dims multiple of 4
		scaledWidth += (4 - (scaledWidth % 4)) % 4;
		scaledHeight += (4 - (scaledHeight % 4)) % 4;
		
		//create stuff necessary to draw texture onto a BufferColor at requested size
		BufferColor bufThumb = new BufferColor(gl, scaledWidth, scaledHeight);
		Quad<GL2,GLBuffer> quad = new Quad<GL2,GLBuffer>(gl, GLBuffer.getConstructor());
		ShaderQuad shader = new ShaderQuad(gl, "Copy quad shader");
		
		//prepare bufThumb for drawing to
		bufThumb.clearColor(gl, new Vector4d(1,0,1,1));
		bufThumb.bindDrawTo(gl);
		//draw this texture onto bufThumb (this is exactly drawBuffer in main Renderer routine)
		shader.useShader(gl);
		shader.setVertexAttributeBuffers(gl, quad);
		bind(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		gl.glDrawArrays(GL2.GL_TRIANGLES, 0, 3*quad.getTriCount());
		unbind(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		shader.unuseShader(gl);
		//unbind bufThumb
		bufThumb.unbindDrawTo(gl);
		
		//read bufThumb into target BufferTexture
		ByteBuffer data = target.getBufferForWriting(scaledWidth, scaledHeight);
		
		bufThumb.bindReadFrom(gl);
		gl.glReadPixels(0, 0, scaledWidth, scaledHeight, 
			GL2.GL_BGRA, GL2.GL_UNSIGNED_BYTE, data);
		bufThumb.unbindReadFrom(gl);
		
		//delete created stuff
		bufThumb.delete(gl);
		//[TODO] delete quad
		//[TODO] delete shader
	}
}
