package engine.renderer.forwardjogl.shader;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import javax.media.opengl.GL2ES2;

import engine.renderer.shadertools.GLSLESPreprocessor;
import geometry.common.MessageOutput;


public abstract class ShaderProgram{
	private static final boolean CHECK_ERRORS = true;
	
	private String name; //just used for outputting debug info/errors
	
	protected int shaderProgramID;
	private ArrayList<Integer> vertexAttributes;
	
	//specified sources for preprocessing [only needed until finishAttaching is called]
	private InputStream primaryVertexShaderSource, primaryFragmentShaderSource;
	private HashMap<String,InputStream> vertexShaderSourceIdentifiers, fragmentShaderSourceIdentifiers;
	
	
	public ShaderProgram(GL2ES2 gl, String name){ 
		this.name = name;
		
		vertexAttributes = new ArrayList<Integer>();
		shaderProgramID = gl.glCreateProgram();
		
		vertexShaderSourceIdentifiers = new HashMap<String,InputStream>();
		fragmentShaderSourceIdentifiers = new HashMap<String,InputStream>();
	}
	
	
	protected void setPrimaryVertexShaderSource(InputStream source){
		primaryVertexShaderSource = source;
	}
	
	protected void setPrimaryFragmentShaderSource(InputStream source){
		primaryFragmentShaderSource = source;
	}
	
	protected void addVertexShaderSourceIdentifier(String identifier, InputStream source){
		vertexShaderSourceIdentifiers.put(identifier, source);
	}
	
	protected void addFragmentShaderSourceIdentifier(String identifier, InputStream source){
		fragmentShaderSourceIdentifiers.put(identifier, source);
	}
	
	
	protected void finishAttaching(GL2ES2 gl){
		
		//do preprocessing to link multiple sources (necessary because of GLES 2.0); attach resulting source
		String vertexShaderSource = GLSLESPreprocessor.process(primaryVertexShaderSource, vertexShaderSourceIdentifiers);
		VertexShader vs = new VertexShader(gl, vertexShaderSource);
		gl.glAttachShader(shaderProgramID, vs.getID());
		
		String fragmentShaderSource = GLSLESPreprocessor.process(primaryFragmentShaderSource, fragmentShaderSourceIdentifiers);
		FragmentShader fs = new FragmentShader(gl, fragmentShaderSource);
		gl.glAttachShader(shaderProgramID, fs.getID());
		
		//bind vertex attributes
		setVertexAttributes(gl);
		
		//link the shader program
		MessageOutput.printDebug("Linking program (" + name + ")");
		gl.glLinkProgram(shaderProgramID);
		if(CHECK_ERRORS){ checkLinkStatus(gl); }

		//set up uniforms that will be used
		setUniforms(gl);
	}

	
	protected abstract void setVertexAttributes(GL2ES2 gl);
	
	protected void addVertexAttribute(GL2ES2 gl, String name, int index){
		vertexAttributes.add(index);
		gl.glBindAttribLocation(shaderProgramID, index, name);
	}
	
	protected abstract void setUniforms(GL2ES2 gl);

	public void useShader(GL2ES2 gl){
		
		//enable shader
		gl.glUseProgram(shaderProgramID);
		//enable attributes
		for(int index : vertexAttributes){ gl.glEnableVertexAttribArray(index); }
	}
	
	public void unuseShader(GL2ES2 gl){
		
		//disable attributes
		for(int index : vertexAttributes){ gl.glDisableVertexAttribArray(index); }
		//disable shader
		gl.glUseProgram(0);
	}
	
    
    public void checkLinkStatus(GL2ES2 gl){
		
        IntBuffer status = IntBuffer.allocate(1);
        gl.glGetProgramiv(shaderProgramID, GL2ES2.GL_LINK_STATUS, status);

        MessageOutput.printDebug("Checking GL_LINK_STATUS (" + name + ")");
        if(status.get() == GL2ES2.GL_FALSE){
        	MessageOutput.printDebug("GL_LINK_STATUS is GL_FALSE! Info log: ");
        	throw new Error(getProgramInfoLog(gl));
        }else{
        	MessageOutput.printDebug("GL_LINK_STATUS is ok. Info log: \n" + getProgramInfoLog(gl));
        }
    }

    public void checkValidateStatus(GL2ES2 gl){
		
		IntBuffer status = IntBuffer.allocate(1); 
		gl.glValidateProgram(shaderProgramID);
		gl.glGetProgramiv(shaderProgramID, GL2ES2.GL_VALIDATE_STATUS, status);
		
		MessageOutput.printDebug("Checking GL_VALIDATE_STATUS (" + name + ")");
		if(status.get() == GL2ES2.GL_FALSE){
			MessageOutput.printDebug("GL_VALIDATE_STATUS is GL_FALSE! Info log: ");
			throw new Error(getProgramInfoLog(gl));
		}else{
			MessageOutput.printDebug("GL_VALIDATE_STATUS is ok. Info log: \n" + getProgramInfoLog(gl));
		}	
	}
    
    public String getProgramInfoLog(GL2ES2 gl){
    	
    	IntBuffer infoLogLength = IntBuffer.allocate(1);
        gl.glGetProgramiv(shaderProgramID, GL2ES2.GL_INFO_LOG_LENGTH, infoLogLength);
        
        ByteBuffer infoLog = ByteBuffer.allocate(infoLogLength.get(0)); 
        gl.glGetProgramInfoLog(shaderProgramID, infoLogLength.get(0), null, infoLog);

        return Charset.forName("US-ASCII").decode(infoLog).toString();
    }
}
