package engine.renderer.forwardjogl.shader;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;

import javax.media.opengl.GL2ES2;

import geometry.common.MessageOutput;


public abstract class Shader{
	protected static final boolean CHECK_ERRORS = true;
	
	protected int shaderID;
	
	
	protected Shader(int shaderID){
		this.shaderID = shaderID;
	}
	
	public int getID(){ return shaderID; }
	
	
	public void checkCompileStatus(GL2ES2 gl){
		
        IntBuffer status = IntBuffer.allocate(1);
        gl.glGetShaderiv(shaderID, GL2ES2.GL_COMPILE_STATUS, status);
        
        if(status.get() == GL2ES2.GL_FALSE){
        	MessageOutput.printDebug("GL_COMPILE_STATUS is GL_FALSE! Info log: ");
        	throw new Error(getShaderInfoLog(gl));
        }else{
        	MessageOutput.printDebug("GL_COMPILE_STATUS is ok. Info log: \n" + getShaderInfoLog(gl));
        }
    }
	
	public String getShaderInfoLog(GL2ES2 gl){
    	
        IntBuffer infoLogLength = IntBuffer.allocate(1);
        gl.glGetShaderiv(shaderID, GL2ES2.GL_INFO_LOG_LENGTH, infoLogLength);

        ByteBuffer infoLog = ByteBuffer.allocate(infoLogLength.get(0));
        gl.glGetShaderInfoLog(shaderID, infoLogLength.get(0), null, infoLog);

        return Charset.forName("US-ASCII").decode(infoLog).toString();
    }
}
