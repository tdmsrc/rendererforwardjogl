package engine.renderer.forwardjogl.shader.terrain;

import javax.media.opengl.GL2ES2;


public class ShaderTerrainShadowMap extends ShaderTerrainGeneric{

	public ShaderTerrainShadowMap(GL2ES2 gl) {
		super(gl, "Shadow map terrain shader");
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainShadowMapVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainShadowMapFrag.glsl"));
		
		finishAttaching(gl);
	}
}
