package engine.renderer.forwardjogl.shader.terrain;

import javax.media.opengl.GL2ES2;

import geometry.math.Vector3d;


public class ShaderTerrainZPrepass extends ShaderTerrainGeneric{

	//user-set opacity
	private int opacityUniform;
	//fog color
	private int fogColorUniform, fogDensityUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderTerrainZPrepass(GL2ES2 gl) {
		super(gl, "Z-Prepass terrain shader");
		
		addVertexShaderSourceIdentifier("getFogIntensity", getClass().getResourceAsStream("/resource/shader/lightpass/FogIntensity.glsl"));
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainZPrepassVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainZPrepassFrag.glsl"));
		
		finishAttaching(gl);
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
	
	    opacityUniform = gl.glGetUniformLocation(shaderProgramID, "opacity");
	    
	    fogColorUniform = gl.glGetUniformLocation(shaderProgramID, "fogColor");
	    fogDensityUniform = gl.glGetUniformLocation(shaderProgramID, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setOpacity(GL2ES2 gl, float opacity){
		
		//set opacity uniform
		gl.glUniform1f(opacityUniform, opacity);
	}

	public void setFog(GL2ES2 gl, Vector3d fogColor, float fogDensity){
		
		//set fog color uniform
		gl.glUniform3f(fogColorUniform, fogColor.getX(), fogColor.getY(), fogColor.getZ());
		gl.glUniform1f(fogDensityUniform, fogDensity);
	}
}
