package engine.renderer.forwardjogl.shader.terrain;

import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.shader.ShaderProgram;
import engine.terrain.TerrainMetrics;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.math.Vector3d;


public abstract class ShaderTerrainGeneric extends ShaderProgram{
	
	public static final int HEIGHT_AND_AUX_TEXTURE_UNIT = 1;

	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0;
	
	//height data
	private int textureHeightAndAuxUniform;
	//transformations
	private float[] projectionMatrixArray, modelViewRotationMatrixArray;
	private int projectionMatrixUniform, modelViewRotationMatrixUniform, eyePositionUniform;
	//terrain metrics
	private int terrainPosMinUniform, terrainPosMaxUniform;
	private int terrainDataTexMinUniform, terrainDataTexMaxUniform;
	private int terrainColorTexMinUniform, terrainColorTexMaxUniform;
	private int terrainHeightScaleUniform;
	//tile metrics
	private int tileLerpMinUniform, tileLerpMaxUniform;

	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderTerrainGeneric(GL2ES2 gl, String name){
		super(gl, name);
		
		addVertexShaderSourceIdentifier("extractData", getClass().getResourceAsStream("/resource/shader/terrain/GenericTerrainVert.glsl"));
		
		projectionMatrixArray = new float[16];
		modelViewRotationMatrixArray = new float[9];
	}

	@Override
	protected void setVertexAttributes(GL2ES2 gl) {

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {

		//height data
		textureHeightAndAuxUniform = gl.glGetUniformLocation(shaderProgramID, "texHeightAndAux");
		
		//get uniform locations
	    projectionMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "projectionMatrix");
	    modelViewRotationMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "modelViewRotation");
	    eyePositionUniform = gl.glGetUniformLocation(shaderProgramID, "eyePosition");

	    //terrain metric uniforms
	    terrainPosMinUniform = gl.glGetUniformLocation(shaderProgramID, "terrainPosMin");
	    terrainPosMaxUniform = gl.glGetUniformLocation(shaderProgramID, "terrainPosMax");
	    terrainDataTexMinUniform = gl.glGetUniformLocation(shaderProgramID, "terrainDataTexMin");
	    terrainDataTexMaxUniform = gl.glGetUniformLocation(shaderProgramID, "terrainDataTexMax");
	    terrainColorTexMinUniform = gl.glGetUniformLocation(shaderProgramID, "terrainColorTexMin");
	    terrainColorTexMaxUniform = gl.glGetUniformLocation(shaderProgramID, "terrainColorTexMax");
	    terrainHeightScaleUniform = gl.glGetUniformLocation(shaderProgramID, "terrainHeightScale");
	    
	    //tile lerp uniforms
	    tileLerpMinUniform = gl.glGetUniformLocation(shaderProgramID, "tileLerpMin");
	    tileLerpMaxUniform = gl.glGetUniformLocation(shaderProgramID, "tileLerpMax");   
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);

		//set textures
		gl.glUniform1i(textureHeightAndAuxUniform, HEIGHT_AND_AUX_TEXTURE_UNIT);
	}
	
	public void setViewMatrices(GL2ES2 gl, Camera camera){
		
		camera.getProjectionMatrix().serializeColumnMajor(projectionMatrixArray);
		gl.glUniformMatrix4fv(projectionMatrixUniform, 1, false, projectionMatrixArray, 0);
		
		camera.getRotation().getMatrix().serializeColumnMajor(modelViewRotationMatrixArray);
		gl.glUniformMatrix3fv(modelViewRotationMatrixUniform, 1, false, modelViewRotationMatrixArray, 0);
		
		Vector3d eyePosition = camera.getPosition();
		gl.glUniform3f(eyePositionUniform, eyePosition.getX(), eyePosition.getY(), eyePosition.getZ());
	}
	
	public void setTerrainMetrics(GL2ES2 gl, TerrainMetrics terrainMetrics){
		
		gl.glUniform1f(terrainHeightScaleUniform, terrainMetrics.getHeightScale());	
		
		Vector2d terrainPosMin = terrainMetrics.getTerrainPosBounds().getMin();
		gl.glUniform2f(terrainPosMinUniform, terrainPosMin.getX(), terrainPosMin.getY());
		
		Vector2d terrainPosMax = terrainMetrics.getTerrainPosBounds().getMax();
		gl.glUniform2f(terrainPosMaxUniform, terrainPosMax.getX(), terrainPosMax.getY());
		
		Vector2d terrainDataTexMin = terrainMetrics.getTerrainDataTexBounds().getMin();
		gl.glUniform2f(terrainDataTexMinUniform, terrainDataTexMin.getX(), terrainDataTexMin.getY());
		
		Vector2d terrainDataTexMax = terrainMetrics.getTerrainDataTexBounds().getMax();
		gl.glUniform2f(terrainDataTexMaxUniform, terrainDataTexMax.getX(), terrainDataTexMax.getY());
		
		Vector2d terrainColorTexMin = terrainMetrics.getTerrainColorTexBounds().getMin();
		gl.glUniform2f(terrainColorTexMinUniform, terrainColorTexMin.getX(), terrainColorTexMin.getY());
		
		Vector2d terrainColorTexMax = terrainMetrics.getTerrainColorTexBounds().getMax();
		gl.glUniform2f(terrainColorTexMaxUniform, terrainColorTexMax.getX(), terrainColorTexMax.getY());
	}
	
	public void setVertexAttributeBuffers(GL2ES2 gl, GLBuffer tileData){

		//bind vertex data that the tile IBOs will use
		tileData.bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
	}
	
	public void setTileLerp(GL2ES2 gl, Vector2d tileLerpMin, Vector2d tileLerpMax){
		
		gl.glUniform2f(tileLerpMinUniform, tileLerpMin.getX(), tileLerpMin.getY());
		gl.glUniform2f(tileLerpMaxUniform, tileLerpMax.getX(), tileLerpMax.getY());
	}
}
