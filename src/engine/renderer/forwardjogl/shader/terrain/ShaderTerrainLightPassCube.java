package engine.renderer.forwardjogl.shader.terrain;

import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.shader.lightpass.ShaderLightPassCube;


public class ShaderTerrainLightPassCube extends ShaderTerrainLightPass{

	public static final int SHADOW_MAP_TEXTURE_UNIT = ShaderLightPassCube.SHADOW_MAP_TEXTURE_UNIT;
	
	private int textureShadowMapCubeUniform;
	private int cubeProjZNearUniform, cubeProjZFarUniform; 
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderTerrainLightPassCube(GL2ES2 gl){
		super(gl);
	}
	
	@Override
	protected void attachShadowShader(GL2ES2 gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", getClass().getResourceAsStream("/resource/shader/lightpass/IsShadowedCube.glsl"));
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		textureShadowMapCubeUniform = gl.glGetUniformLocation(shaderProgramID, "texShadowMapCube");
		
		cubeProjZNearUniform = gl.glGetUniformLocation(shaderProgramID, "cubeProjZNear");
		cubeProjZFarUniform = gl.glGetUniformLocation(shaderProgramID, "cubeProjZFar");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);
		
		gl.glUniform1i(textureShadowMapCubeUniform, SHADOW_MAP_TEXTURE_UNIT);
	}
	
	public void setCubeProjZBounds(GL2ES2 gl, float zNear, float zFar){
		
		gl.glUniform1f(cubeProjZNearUniform, zNear);
		gl.glUniform1f(cubeProjZFarUniform, zFar);
	}
}
