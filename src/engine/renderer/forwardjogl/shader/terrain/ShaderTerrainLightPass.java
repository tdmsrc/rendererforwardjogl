package engine.renderer.forwardjogl.shader.terrain;

import javax.media.opengl.GL2ES2;

import engine.renderer.Material;
import engine.scene.Light;
import geometry.math.Vector3d;

public class ShaderTerrainLightPass extends ShaderTerrainGeneric{

	public static final int NORMAL_AND_BLEND_TEXTURE_UNIT = 2;
	
	public static final int COLOR1_TEXTURE_UNIT = 3;
	public static final int COLOR2_TEXTURE_UNIT = 4;
	
	//texture data
	private int textureNormalAndBlendUniform; 
	private int textureColor1Uniform, textureColor2Uniform;
	//material data
	private int diffuseCoefficientUniform, specularCoefficientUniform, specularExponentUniform;
	//light options
	private int lightPositionUniform, lightColorUniform, lightAttenuationUniform;
	private int fogDensityUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderTerrainLightPass(GL2ES2 gl){
		super(gl, "Light pass terrain shader");
		
		addVertexShaderSourceIdentifier("getFogIntensity", getClass().getResourceAsStream("/resource/shader/lightpass/FogIntensity.glsl"));
		addFragmentShaderSourceIdentifier("getPhong", getClass().getResourceAsStream("/resource/shader/lightpass/PhongFunction.glsl"));
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainLightPassVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainLightPassFrag.glsl"));
		
		attachShadowShader(gl);
		//addFragmentShaderSourceIdentifier("shadowFilter", getClass().getResourceAsStream("/resource/shader/lightpass/ShadowFilterNone.glsl"));
		addFragmentShaderSourceIdentifier("shadowFilter", getClass().getResourceAsStream("/resource/shader/lightpass/ShadowFilterPCF.glsl"));
		
		finishAttaching(gl);
	}
	
	protected void attachShadowShader(GL2ES2 gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", getClass().getResourceAsStream("/resource/shader/lightpass/IsShadowedNever.glsl"));
	}

	@Override
	protected void setUniforms(GL2ES2 gl){
		super.setUniforms(gl);
	
		//additional textures
		textureNormalAndBlendUniform = gl.glGetUniformLocation(shaderProgramID, "texNormalAndBlend");
	    textureColor1Uniform = gl.glGetUniformLocation(shaderProgramID, "texColor1");
	    textureColor2Uniform = gl.glGetUniformLocation(shaderProgramID, "texColor2");
	    
	    //material
	    diffuseCoefficientUniform = gl.glGetUniformLocation(shaderProgramID, "mtlDiffuseCoefficient");
	    specularCoefficientUniform = gl.glGetUniformLocation(shaderProgramID, "mtlSpecularCoefficient");
	    specularExponentUniform = gl.glGetUniformLocation(shaderProgramID, "mtlSpecularExponent");
	    
		//light uniforms
		lightPositionUniform = gl.glGetUniformLocation(shaderProgramID, "lightPosition");
	    lightColorUniform = gl.glGetUniformLocation(shaderProgramID, "lightColor");
	    lightAttenuationUniform = gl.glGetUniformLocation(shaderProgramID, "lightAttenuation");
	    
	    fogDensityUniform = gl.glGetUniformLocation(shaderProgramID, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);
		
		//set textures
		gl.glUniform1i(textureNormalAndBlendUniform, NORMAL_AND_BLEND_TEXTURE_UNIT);
		
		gl.glUniform1i(textureColor1Uniform, COLOR1_TEXTURE_UNIT);
		gl.glUniform1i(textureColor2Uniform, COLOR2_TEXTURE_UNIT);
	}
	
	public void setMaterialLightingCoefficients(GL2ES2 gl, Material<?> material){
		
		//set material property uniforms
		gl.glUniform1f(diffuseCoefficientUniform, material.getDiffuseCoefficient());
		gl.glUniform1f(specularCoefficientUniform, material.getSpecularCoefficient());
		gl.glUniform1f(specularExponentUniform, material.getSpecularExponent());
	}
	
	public void setLightAndFog(GL2ES2 gl, Light light, float fogDensity){
		
		//set light position and color, fog density
		Vector3d lightPosition = light.getCamera().getPosition();
		gl.glUniform3f(lightPositionUniform, lightPosition.getX(), lightPosition.getY(), lightPosition.getZ());
		
		Vector3d lightColor = light.getColor();
		gl.glUniform3f(lightColorUniform, lightColor.getX(), lightColor.getY(), lightColor.getZ());
		
		Vector3d lightAttenuation = light.getAttenuation();
		gl.glUniform3f(lightAttenuationUniform, lightAttenuation.getX(), lightAttenuation.getY(), lightAttenuation.getZ());
		
		gl.glUniform1f(fogDensityUniform, fogDensity);
	}
}
