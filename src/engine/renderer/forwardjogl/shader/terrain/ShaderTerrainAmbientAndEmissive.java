package engine.renderer.forwardjogl.shader.terrain;

import javax.media.opengl.GL2ES2;

import engine.scene.DrawOptions;
import geometry.math.Vector3d;

public class ShaderTerrainAmbientAndEmissive extends ShaderTerrainGeneric{
	
	protected static Vector3d ZERO_VECTOR = new Vector3d();

	public static final int NORMAL_AND_BLEND_TEXTURE_UNIT = 2;
	
	public static final int COLOR1_TEXTURE_UNIT = 3;
	public static final int COLOR2_TEXTURE_UNIT = 4;
	
	//texture data
	private int textureNormalAndBlendUniform; 
	private int textureColor1Uniform, textureColor2Uniform;
	//light options
	private int ambientCoefficientUniform, ambientColorUniform;
	private int emissiveCoefficientUniform, emissiveColorUniform;
	private int fogDensityUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderTerrainAmbientAndEmissive(GL2ES2 gl){
		super(gl, "Ambient and emissive terrain shader");
		
		addVertexShaderSourceIdentifier("getFogIntensity", getClass().getResourceAsStream("/resource/shader/lightpass/FogIntensity.glsl"));
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainAmbientAndEmissiveVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainAmbientAndEmissiveFrag.glsl"));

		finishAttaching(gl);
	}

	@Override
	protected void setUniforms(GL2ES2 gl){
		super.setUniforms(gl);
	
		//additional textures
		textureNormalAndBlendUniform = gl.glGetUniformLocation(shaderProgramID, "texNormalAndBlend");
	    textureColor1Uniform = gl.glGetUniformLocation(shaderProgramID, "texColor1");
	    textureColor2Uniform = gl.glGetUniformLocation(shaderProgramID, "texColor2");
	    
	    //light uniforms
	    ambientCoefficientUniform = gl.glGetUniformLocation(shaderProgramID, "ambientCoefficient");
	    ambientColorUniform = gl.glGetUniformLocation(shaderProgramID, "ambientColor");
	    emissiveCoefficientUniform = gl.glGetUniformLocation(shaderProgramID, "emissiveCoefficient");
	    emissiveColorUniform = gl.glGetUniformLocation(shaderProgramID, "emissiveColor");
	    
	    fogDensityUniform = gl.glGetUniformLocation(shaderProgramID, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);
		
		//set textures
		gl.glUniform1i(textureNormalAndBlendUniform, NORMAL_AND_BLEND_TEXTURE_UNIT);
		
		gl.glUniform1i(textureColor1Uniform, COLOR1_TEXTURE_UNIT);
		gl.glUniform1i(textureColor2Uniform, COLOR2_TEXTURE_UNIT);
	}
	
	public void setFog(GL2ES2 gl, float fogDensity){
		
		//set fog density
		gl.glUniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setAmbient(GL2ES2 gl, Vector3d ambientColor, float ambientCoefficient){
		
		//set material lighting coefficient uniforms
		gl.glUniform3f(ambientColorUniform, ambientColor.getX(), ambientColor.getY(), ambientColor.getZ());
		gl.glUniform1f(ambientCoefficientUniform, ambientCoefficient);
	}
	
	public void setEmissive(GL2ES2 gl, DrawOptions drawOptions){
		
		if(drawOptions.checkFlags(DrawOptions.MASK_EMISSIVE, DrawOptions.MASK_EMISSIVE)){
			Vector3d emissiveColor = drawOptions.getEmissiveColor();
			gl.glUniform3f(emissiveColorUniform, emissiveColor.getX(), emissiveColor.getY(), emissiveColor.getZ());
			gl.glUniform1f(emissiveCoefficientUniform, drawOptions.getEmissiveCoefficient());
		}else{
			gl.glUniform3f(emissiveColorUniform, ZERO_VECTOR.getX(), ZERO_VECTOR.getY(), ZERO_VECTOR.getZ());
			gl.glUniform1f(emissiveCoefficientUniform, 0.0f);
		}
	}
}
