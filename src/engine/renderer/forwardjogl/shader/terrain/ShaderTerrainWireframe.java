package engine.renderer.forwardjogl.shader.terrain;

import javax.media.opengl.GL2ES2;

import geometry.math.Vector3d;


public class ShaderTerrainWireframe extends ShaderTerrainGeneric{

	//wireframe color
	private int colorUniform;
		
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderTerrainWireframe(GL2ES2 gl){
		super(gl, "Wireframe terrain shader");
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainWireframeVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/terrain/ShaderTerrainWireframeFrag.glsl"));
		
		finishAttaching(gl);
	}
	
	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);

		colorUniform = gl.glGetUniformLocation(shaderProgramID, "wireframeColor");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setColor(GL2ES2 gl, Vector3d color){
		
		//set color uniform
		gl.glUniform3f(colorUniform, color.getX(), color.getY(), color.getZ());
	}
}
