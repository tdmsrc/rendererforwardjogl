package engine.renderer.forwardjogl.shader.skybox;

import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.shader.ShaderProgram;
import engine.scene.Skybox;
import engine.scene.Skybox.SkyboxFace;
import geometry.math.Camera;


public class ShaderSkybox extends ShaderProgram{

	public static final int SKYBOX_TEXTURE_UNIT = 0;
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1;
	
	//texture
	private int textureColorUniform;
	//transformations
	private float[] projectionMatrixArray, modelViewRotationMatrixArray;
	private int projectionMatrixUniform, modelViewRotationMatrixUniform;
	

	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderSkybox(GL2ES2 gl){
		super(gl, "Skybox shader");
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/skybox/ShaderSkyboxVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/skybox/ShaderSkyboxFrag.glsl"));
		finishAttaching(gl);
		
		projectionMatrixArray = new float[16];
		modelViewRotationMatrixArray = new float[9];
	}

	@Override
	protected void setVertexAttributes(GL2ES2 gl){

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
	}

	@Override
	protected void setUniforms(GL2ES2 gl){

		//get uniform locations
		textureColorUniform = gl.glGetUniformLocation(shaderProgramID, "texColor");
		
	    projectionMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "projectionMatrix");
	    modelViewRotationMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "modelViewRotation"); 
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);

		//set textures
		gl.glUniform1i(textureColorUniform, SKYBOX_TEXTURE_UNIT);
	}
	
	public void setViewMatrices(GL2ES2 gl, Camera camera){
		
		camera.getProjectionMatrix().serializeColumnMajor(projectionMatrixArray);
		gl.glUniformMatrix4fv(projectionMatrixUniform, 1, false, projectionMatrixArray, 0);
		
		camera.getRotation().getMatrix().serializeColumnMajor(modelViewRotationMatrixArray);
		gl.glUniformMatrix3fv(modelViewRotationMatrixUniform, 1, false, modelViewRotationMatrixArray, 0);
	}
	
	public void setVertexAttributeBuffers(GL2ES2 gl, Skybox<?,GLBuffer,?> skybox, SkyboxFace face){

		//bind vertex data
		skybox.getPositionBuffer(face).bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		skybox.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
	
	public void setVertexAttributeBuffersSun(GL2ES2 gl, Skybox<?,GLBuffer,?> skybox){
		
		//bind vertex data
		skybox.getSunPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		skybox.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
}
