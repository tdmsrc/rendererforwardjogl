package engine.renderer.forwardjogl.shader.quad;

import javax.media.opengl.GL2ES2;

import geometry.math.Vector2d;

public class ShaderQuadBlurRadial extends ShaderQuad{
	
	//adjustment parameters
	private int blurCenterUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadBlurRadial(GL2ES2 gl){
		super(gl, "Radial blur quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragBlurRadial.glsl"));
	}
	
	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		blurCenterUniform = gl.glGetUniformLocation(shaderProgramID, "blurCenter");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setBlurCenter(GL2ES2 gl, Vector2d blurCenter){
		
		gl.glUniform2f(blurCenterUniform, blurCenter.getX(), blurCenter.getY());
	}
}
