package engine.renderer.forwardjogl.shader.quad;

import javax.media.opengl.GL2ES2;

public class ShaderQuadColorAdjust extends ShaderQuad{

	//adjustment parameters
	private int colorAdjustBrightnessUniform;
	private int colorAdjustContrastUniform;
	private int colorAdjustSaturationUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadColorAdjust(GL2ES2 gl){
		super(gl, "Color adjust quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragColorAdjust.glsl"));
		
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		colorAdjustBrightnessUniform = gl.glGetUniformLocation(shaderProgramID, "colorAdjustBrightness");
		colorAdjustContrastUniform = gl.glGetUniformLocation(shaderProgramID, "colorAdjustContrast");
		colorAdjustSaturationUniform = gl.glGetUniformLocation(shaderProgramID, "colorAdjustSaturation");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setColorAdjust(GL2ES2 gl, float colorAdjustBrightness, float colorAdjustContrast, float colorAdjustSaturation){
		
		gl.glUniform1f(colorAdjustBrightnessUniform, colorAdjustBrightness);
		gl.glUniform1f(colorAdjustContrastUniform, colorAdjustContrast);
		gl.glUniform1f(colorAdjustSaturationUniform, colorAdjustSaturation);
	}
}
