package engine.renderer.forwardjogl.shader.quad;

import javax.media.opengl.GL2ES2;

public class ShaderQuadDepthOfField extends ShaderQuad{
	
	public static final int DEPTH_TEXTURE_UNIT = 1;
	public static final int BLUR_TEXTURE_UNIT = 2;

	//depth texture 
	private int depthTextureUniform;
	private int blurTextureUniform;
	
	//adjustment parameters
	private int zNearInvUniform;
	private int zFarInvUniform;
	private int dFocusTargetUniform;
	private int dFocusRangeInvUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadDepthOfField(GL2ES2 gl){
		super(gl, "Depth field quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragDepthOfField.glsl"));
		
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		depthTextureUniform = gl.glGetUniformLocation(shaderProgramID, "texDepth");
		blurTextureUniform = gl.glGetUniformLocation(shaderProgramID, "texBlur");
		
		zNearInvUniform = gl.glGetUniformLocation(shaderProgramID, "zNearInv");
		zFarInvUniform = gl.glGetUniformLocation(shaderProgramID, "zFarInv");
		dFocusTargetUniform = gl.glGetUniformLocation(shaderProgramID, "dFocusTarget");
		dFocusRangeInvUniform = gl.glGetUniformLocation(shaderProgramID, "dFocusRangeInv");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);
		
		//set textures
		gl.glUniform1i(depthTextureUniform, DEPTH_TEXTURE_UNIT);
		gl.glUniform1i(blurTextureUniform, BLUR_TEXTURE_UNIT);
	}
	
	public void setParameters(GL2ES2 gl, float zNear, float zFar, float focusTargetDistance, float focusRange){
		
		gl.glUniform1f(zNearInvUniform, 1.0f / zNear);
		gl.glUniform1f(zFarInvUniform, 1.0f / zFar);
		gl.glUniform1f(dFocusTargetUniform, focusTargetDistance);
		gl.glUniform1f(dFocusRangeInvUniform, 1.0f / focusRange);
	}
}
