package engine.renderer.forwardjogl.shader.quad;

import javax.media.opengl.GL2ES2;

public class ShaderQuadCopyAlpha extends ShaderQuad{
	
	//adjustment parameters
	private int alphaUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadCopyAlpha(GL2ES2 gl){
		super(gl, "Copy alpha quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragCopyAlpha.glsl"));
	}
	
	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		alphaUniform = gl.glGetUniformLocation(shaderProgramID, "alpha");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setAlpha(GL2ES2 gl, float alpha){
		
		gl.glUniform1f(alphaUniform, alpha);
	}
}
