package engine.renderer.forwardjogl.shader.quad;

import javax.media.opengl.GL2ES2;

public class ShaderQuadHSVAdjust extends ShaderQuad{

	//adjustment parameters
	private int hsvAdjustHueUniform;
	private int hsvAdjustSaturationUniform;
	private int hsvAdjustValueUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadHSVAdjust(GL2ES2 gl){
		super(gl, "HSV adjust quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragHSVAdjust.glsl"));
		
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		hsvAdjustHueUniform = gl.glGetUniformLocation(shaderProgramID, "hsvAdjustHue");
		hsvAdjustSaturationUniform = gl.glGetUniformLocation(shaderProgramID, "hsvAdjustSaturation");
		hsvAdjustValueUniform = gl.glGetUniformLocation(shaderProgramID, "hsvAdjustValue");
		
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setHSVAdjust(GL2ES2 gl, float hsvAdjustHue, float hsvAdjustSaturation, float hsvAdjustValue){
		
		gl.glUniform1f(hsvAdjustHueUniform, hsvAdjustHue);
		gl.glUniform1f(hsvAdjustSaturationUniform, hsvAdjustSaturation);
		gl.glUniform1f(hsvAdjustValueUniform, hsvAdjustValue);
		
	}
}
