package engine.renderer.forwardjogl.shader.quad;

import javax.media.opengl.GL2ES2;

public class ShaderQuadSSAO extends ShaderQuad{
	
	public static final int DEPTH_TEXTURE_UNIT = 1;

	//depth texture 
	private int depthTextureUniform;
	
	//adjustment parameters
	private int zNearInvUniform;
	private int zFarInvUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadSSAO(GL2ES2 gl){
		super(gl, "SSAO quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragSSAO.glsl"));
		
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		depthTextureUniform = gl.glGetUniformLocation(shaderProgramID, "texDepth");
		
		zNearInvUniform = gl.glGetUniformLocation(shaderProgramID, "zNearInv");
		zFarInvUniform = gl.glGetUniformLocation(shaderProgramID, "zFarInv");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);
		
		//set textures
		gl.glUniform1i(depthTextureUniform, DEPTH_TEXTURE_UNIT);
	}
	
	public void setParameters(GL2ES2 gl, float zNear, float zFar){
		
		gl.glUniform1f(zNearInvUniform, 1.0f / zNear);
		gl.glUniform1f(zFarInvUniform, 1.0f / zFar);
	}
}
