package engine.renderer.forwardjogl.shader.quad;

import java.io.InputStream;

import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.shader.ShaderProgram;
import engine.tools.Quad;

public class ShaderQuad extends ShaderProgram{

	public static final int COLOR_TEXTURE_UNIT = 0;
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1;

	//texture data
	private int textureColorUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuad(GL2ES2 gl, String name){
		super(gl, name);
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/quad/ShaderQuadVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/quad/ShaderQuadFragCopy.glsl"));
		finishAttaching(gl);
	}
	
	public ShaderQuad(GL2ES2 gl, String name, InputStream streamFragmentShaderSource){
		super(gl, name);
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/quad/ShaderQuadVert.glsl"));
		setPrimaryFragmentShaderSource(streamFragmentShaderSource);
		finishAttaching(gl);
	}
	
	@Override
	protected void setVertexAttributes(GL2ES2 gl) {

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
	}
	
	@Override
	protected void setUniforms(GL2ES2 gl) {
  
		//get uniform locations
	    textureColorUniform = gl.glGetUniformLocation(shaderProgramID, "texColor");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);

		//set texture
		gl.glUniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
	}
	
	public void setVertexAttributeBuffers(GL2ES2 gl, Quad<?,GLBuffer> quad){

		//bind vertex data
		quad.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		quad.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
}
