package engine.renderer.forwardjogl.shader.quad;

import javax.media.opengl.GL2ES2;

public class ShaderQuadBlur extends ShaderQuad{

	//texture data
	private int textureSizeUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public enum BlurDirection{ BLUR_DIRECTION_HORIZONTAL, BLUR_DIRECTION_VERTICAL; }
	
	public ShaderQuadBlur(GL2ES2 gl, BlurDirection blurDirection){
		super(gl, (blurDirection == BlurDirection.BLUR_DIRECTION_HORIZONTAL) ? "BlurH quad shader" : "BlurV quad shader", 
			ShaderQuad.class.getResourceAsStream(
				(blurDirection == BlurDirection.BLUR_DIRECTION_HORIZONTAL) ? 
				"/resource/shader/quad/ShaderQuadFragBlurH.glsl" : 
				"/resource/shader/quad/ShaderQuadFragBlurV.glsl" )
			);
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		textureSizeUniform = gl.glGetUniformLocation(shaderProgramID, "texSize");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setTextureSize(GL2ES2 gl, int textureWidth, int textureHeight){
		
		gl.glUniform2i(textureSizeUniform, textureWidth, textureHeight);
	}
}
