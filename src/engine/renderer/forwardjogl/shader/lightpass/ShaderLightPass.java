package engine.renderer.forwardjogl.shader.lightpass;

import javax.media.opengl.GL2ES2;

import engine.renderer.Drawable;
import engine.renderer.Material;
import engine.renderer.forwardjogl.object.GLBuffer;
import engine.scene.Light;
import geometry.math.Vector3d;


public class ShaderLightPass extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1;
	public static final int BUMP_TEXTURE_UNIT = 2; //[TODO] only used if normalMapping == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1,
		VERTEX_NORMAL_ATTRIBUTE = 2,
		VERTEX_TANGENT_S_ATTRIBUTE = 3, //[TODO] only used if normalMapping == true
		VERTEX_TANGENT_T_ATTRIBUTE = 4; //[TODO] only used if normalMapping == true
	
	//options for attaching shaders
	private static final OptionShadowFilter optionShadowFilter = OptionShadowFilter.PERCENTAGE_CLOSER_FILTERING;
	private static final OptionParallax optionParallax = OptionParallax.PARALLAX_OCCLUSION_MAPPING;
	
	//texture/material data
	private int textureColorUniform;
	private int textureBumpUniform; //[TODO] only used if normalMapping == true
	private int reliefMappingHeightUniform; //[TODO] only used if normalMapping == true
	private int diffuseCoefficientUniform, specularCoefficientUniform, specularExponentUniform;
	//light options
	private int lightPositionUniform, lightColorUniform, lightAttenuationUniform;
	private int fogDensityUniform;
	
	//shader options
	private boolean normalMapping;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderLightPass(GL2ES2 gl, boolean normalMapping){
		super(gl, "Light pass shader [normalMapping: " + normalMapping + "]");
		
		this.normalMapping = normalMapping;
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderLightPassVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderLightPassFrag.glsl"));
		
		addVertexShaderSourceIdentifier("getFogIntensity", getClass().getResourceAsStream("/resource/shader/lightpass/FogIntensity.glsl"));
		addFragmentShaderSourceIdentifier("getPhong", getClass().getResourceAsStream("/resource/shader/lightpass/PhongFunction.glsl"));
		
		switch(optionShadowFilter){
		case NONE:
			addFragmentShaderSourceIdentifier("shadowFilter", getClass().getResourceAsStream("/resource/shader/lightpass/ShadowFilterNone.glsl")); break;
		case PERCENTAGE_CLOSER_FILTERING:
			addFragmentShaderSourceIdentifier("shadowFilter", getClass().getResourceAsStream("/resource/shader/lightpass/ShadowFilterPCF.glsl")); break;
		}
		
		attachShadowShaders(gl);
		
		if(normalMapping){
			addVertexShaderSourceIdentifier("setTangentVectors", getClass().getResourceAsStream("/resource/shader/lightpass/VaryingTangentsTextureVert.glsl"));
			addFragmentShaderSourceIdentifier("getBumpedNormal", getClass().getResourceAsStream("/resource/shader/lightpass/BumpedNormalTextureFrag.glsl"));
						
			switch(optionParallax){
			case NONE:
				addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxNone.glsl")); break;
			case PARALLAX_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxMapping.glsl")); break;
			case PARALLAX_OCCLUSION_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxOcclusionMapping.glsl")); break;
			}
		}else{
			addVertexShaderSourceIdentifier("setTangentVectors", getClass().getResourceAsStream("/resource/shader/lightpass/VaryingTangentsNoneVert.glsl"));
			addFragmentShaderSourceIdentifier("getBumpedNormal", getClass().getResourceAsStream("/resource/shader/lightpass/BumpedNormalNoneFrag.glsl"));
			
			addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxNone.glsl"));
		}
		
		finishAttaching(gl);
	}
	
	protected void attachShadowShaders(GL2ES2 gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", getClass().getResourceAsStream("/resource/shader/lightpass/IsShadowedNever.glsl"));
	}

	@Override
	protected void setVertexAttributes(GL2ES2 gl) {
		
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		addVertexAttribute(gl, "vertexNormal", VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){ 
			addVertexAttribute(gl, "vertexTangentS", VERTEX_TANGENT_S_ATTRIBUTE);
			addVertexAttribute(gl, "vertexTangentT", VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
	    super.setUniforms(gl);
		
	    textureColorUniform = gl.glGetUniformLocation(shaderProgramID, "texColor");
	    if(normalMapping){ 
	    	textureBumpUniform = gl.glGetUniformLocation(shaderProgramID, "texBump"); 
	    	reliefMappingHeightUniform = gl.glGetUniformLocation(shaderProgramID, "reliefMappingHeight");
	    }
	    
	    //material
	    diffuseCoefficientUniform = gl.glGetUniformLocation(shaderProgramID, "mtlDiffuseCoefficient");
	    specularCoefficientUniform = gl.glGetUniformLocation(shaderProgramID, "mtlSpecularCoefficient");
	    specularExponentUniform = gl.glGetUniformLocation(shaderProgramID, "mtlSpecularExponent");
	    
	    //light uniforms
		lightPositionUniform = gl.glGetUniformLocation(shaderProgramID, "lightPosition");
	    lightColorUniform = gl.glGetUniformLocation(shaderProgramID, "lightColor");
	    lightAttenuationUniform = gl.glGetUniformLocation(shaderProgramID, "lightAttenuation");
	    
	    fogDensityUniform = gl.glGetUniformLocation(shaderProgramID, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);

		//set texture
		gl.glUniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		if(normalMapping){
			gl.glUniform1i(textureBumpUniform, BUMP_TEXTURE_UNIT);
		}
	}
	
	public void setLightAndFog(GL2ES2 gl, Light light, float fogDensity){
		
		//set light position and color, fog density
		Vector3d lightPosition = light.getCamera().getPosition();
		gl.glUniform3f(lightPositionUniform, lightPosition.getX(), lightPosition.getY(), lightPosition.getZ());
		
		Vector3d lightColor = light.getColor();
		gl.glUniform3f(lightColorUniform, lightColor.getX(), lightColor.getY(), lightColor.getZ());
		
		Vector3d lightAttenuation = light.getAttenuation();
		gl.glUniform3f(lightAttenuationUniform, lightAttenuation.getX(), lightAttenuation.getY(), lightAttenuation.getZ());
		
		gl.glUniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setMaterialLightingCoefficients(GL2ES2 gl, Material<?> material){
		
		//set material lighting coefficient uniforms
		gl.glUniform1f(diffuseCoefficientUniform, material.getDiffuseCoefficient());
		gl.glUniform1f(specularCoefficientUniform, material.getSpecularCoefficient());
		gl.glUniform1f(specularExponentUniform, material.getSpecularExponent());
	}
	
	public void setReliefMappingHeight(GL2ES2 gl, float reliefMappingHeight){
		
		//set relief mapping height
		gl.glUniform1f(reliefMappingHeightUniform, reliefMappingHeight);
	}
	
	public void setVertexAttributeBuffers(GL2ES2 gl, Drawable<?,GLBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		drawable.getNormalBuffer().bindAsVertexAttrib(gl, VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){
			drawable.getTangentSBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_S_ATTRIBUTE);
			drawable.getTangentTBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}
}
