package engine.renderer.forwardjogl.shader.lightpass;

import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.shader.ShaderProgram;
import engine.scene.SceneObject;
import geometry.math.Camera;
import geometry.math.Matrix3d;
import geometry.math.Vector3d;

public abstract class ShaderGeneric extends ShaderProgram{
	
	protected static Matrix3d IDENTITY_MATRIX = Matrix3d.createIdentityMatrix();
	protected static Vector3d ZERO_VECTOR = new Vector3d();
	
	//options
	public static enum OptionParallax{
		NONE, PARALLAX_MAPPING, PARALLAX_OCCLUSION_MAPPING
	}
	
	public static enum OptionShadowFilter{
		NONE, PERCENTAGE_CLOSER_FILTERING
	}
	
	//transformations
	private float[] projectionMatrixArray, modelViewRotationMatrixArray;
	private int projectionMatrixUniform, modelViewRotationMatrixUniform, eyePositionUniform;
	
	private float[] objRotationMatrixArray;
	private int objRotationMatrixUniform, objTranslationUniform, objOffsetUniform;
	
	//identity matrix entries in column-major order, used when object is not transformed
	private float[] identityMatrixArray; 
	
	//for billboarding
	private float[] modelViewRotationInverseMatrixArray;
	private int useBillboardUniform, modelViewRotationInverseMatrixUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderGeneric(GL2ES2 gl, String name){
		super(gl, name);
		//addVertexShaderSourceIdentifier("getObjRotation", getClass().getResourceAsStream("/resource/shader/lightpass/ObjRotation.glsl"));
		addVertexShaderSourceIdentifier("getObjRotation", getClass().getResourceAsStream("/resource/shader/lightpass/ObjRotationBillboard.glsl"));
		
		projectionMatrixArray = new float[16];
		modelViewRotationMatrixArray = new float[9];
		objRotationMatrixArray = new float[9];
		
		modelViewRotationInverseMatrixArray = new float[9];
		
		identityMatrixArray = new float[9];
		IDENTITY_MATRIX.serializeColumnMajor(identityMatrixArray);
	}
	
	@Override
	protected void setUniforms(GL2ES2 gl) {
		
		//get uniform locations
	    projectionMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "projectionMatrix");
	    modelViewRotationMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "modelViewRotation");
	    eyePositionUniform = gl.glGetUniformLocation(shaderProgramID, "eyePosition");
	    
	    objRotationMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "objRotation");
	    objTranslationUniform = gl.glGetUniformLocation(shaderProgramID, "objTranslation");
	    objOffsetUniform = gl.glGetUniformLocation(shaderProgramID, "objOffset");
	    
	    useBillboardUniform = gl.glGetUniformLocation(shaderProgramID, "useBillboard");
	    modelViewRotationInverseMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "modelViewRotationInverse");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setViewMatrices(GL2ES2 gl, Camera camera){
		
		camera.getProjectionMatrix().serializeColumnMajor(projectionMatrixArray);
		gl.glUniformMatrix4fv(projectionMatrixUniform, 1, false, projectionMatrixArray, 0);
		
		camera.getRotation().getMatrix().serializeColumnMajor(modelViewRotationMatrixArray);
		gl.glUniformMatrix3fv(modelViewRotationMatrixUniform, 1, false, modelViewRotationMatrixArray, 0);
		
		Vector3d eyePosition = camera.getPosition();
		gl.glUniform3f(eyePositionUniform, eyePosition.getX(), eyePosition.getY(), eyePosition.getZ());
		
		//could actually use glUniformMatrix3fv(modelviewRotationMatrix) with "transpose" set to true
		camera.getRotation().getMatrixInverse().serializeColumnMajor(modelViewRotationInverseMatrixArray);
		gl.glUniformMatrix3fv(modelViewRotationInverseMatrixUniform, 1, false, modelViewRotationInverseMatrixArray, 0);
	}
	
	public void setObjectTransformation(GL2ES2 gl, SceneObject<?,?,?> object){

		if(object.isTransformed()){
			//pull rotation, translation, and offset from object transformation
			object.getObjectTransformation().getRotation().getMatrix().serializeColumnMajor(objRotationMatrixArray);
			gl.glUniformMatrix3fv(objRotationMatrixUniform, 1, false, objRotationMatrixArray, 0);
			
			Vector3d objTrans = object.getObjectTransformation().getTranslation();
			gl.glUniform3f(objTranslationUniform, objTrans.getX(), objTrans.getY(), objTrans.getZ());
			
			Vector3d objOffset = object.getObjectTransformation().getOffset();
			gl.glUniform3f(objOffsetUniform, objOffset.getX(), objOffset.getY(), objOffset.getZ());
			
		}else{
			//use identity matrix and zero vectors	
			gl.glUniformMatrix3fv(objRotationMatrixUniform, 1, false, identityMatrixArray, 0);
			gl.glUniform3f(objTranslationUniform, ZERO_VECTOR.getX(), ZERO_VECTOR.getY(), ZERO_VECTOR.getZ());
			gl.glUniform3f(objOffsetUniform, ZERO_VECTOR.getX(), ZERO_VECTOR.getY(), ZERO_VECTOR.getZ());
		}
		
		gl.glUniform1i(useBillboardUniform, object.isBillboard() ? 1 : 0);
	}
}
