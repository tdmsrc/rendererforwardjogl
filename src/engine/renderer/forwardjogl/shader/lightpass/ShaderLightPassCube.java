package engine.renderer.forwardjogl.shader.lightpass;

import javax.media.opengl.GL2ES2;

public class ShaderLightPassCube extends ShaderLightPass{

	public static final int SHADOW_MAP_TEXTURE_UNIT = 0;
	
	private int textureShadowMapCubeUniform;
	private int cubeProjZNearUniform, cubeProjZFarUniform; 
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderLightPassCube(GL2ES2 gl, boolean normalMapping) {
		super(gl, normalMapping);
	}
	
	@Override
	protected void attachShadowShaders(GL2ES2 gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", getClass().getResourceAsStream("/resource/shader/lightpass/IsShadowedCube.glsl"));
	}
	
	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		textureShadowMapCubeUniform = gl.glGetUniformLocation(shaderProgramID, "texShadowMapCube");
		
		cubeProjZNearUniform = gl.glGetUniformLocation(shaderProgramID, "cubeProjZNear");
		cubeProjZFarUniform = gl.glGetUniformLocation(shaderProgramID, "cubeProjZFar");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);
		
		gl.glUniform1i(textureShadowMapCubeUniform, SHADOW_MAP_TEXTURE_UNIT);
	}
	
	public void setCubeProjZBounds(GL2ES2 gl, float zNear, float zFar){
		
		gl.glUniform1f(cubeProjZNearUniform, zNear);
		gl.glUniform1f(cubeProjZFarUniform, zFar);
	}
}
