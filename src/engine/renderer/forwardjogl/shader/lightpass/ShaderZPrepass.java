package engine.renderer.forwardjogl.shader.lightpass;

import javax.media.opengl.GL2ES2;

import engine.renderer.Drawable;
import engine.renderer.forwardjogl.object.GLBuffer;
import geometry.math.Vector3d;


public class ShaderZPrepass extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1; //[TODO] only used if useAlpha == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1; //[TODO] only used if useAlpha == true
	
	//texture/material data
	private int textureColorUniform; //[TODO] only used if useAlpha == true
	private int opacityUniform; //[TODO] only used if useAlpha == true
	//fog color
	private int fogColorUniform, fogDensityUniform;
	
	//shader options
	private boolean useAlpha; 
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderZPrepass(GL2ES2 gl, boolean useAlpha){
		super(gl, "Z-Prepass shader [useAlpha: " + useAlpha + "]");
		
		this.useAlpha = useAlpha;
		
		addVertexShaderSourceIdentifier("getFogIntensity", getClass().getResourceAsStream("/resource/shader/lightpass/FogIntensity.glsl"));
		
		if(useAlpha){
			setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassAlphaVert.glsl"));
			setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassAlphaFrag.glsl"));
		}else{
			setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassNoAlphaVert.glsl"));
			setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassNoAlphaFrag.glsl"));
		}
		
		finishAttaching(gl);
	}
	
	@Override
	protected void setVertexAttributes(GL2ES2 gl) {

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		if(useAlpha){
			addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);

		if(useAlpha){
			textureColorUniform = gl.glGetUniformLocation(shaderProgramID, "texColor");
			opacityUniform = gl.glGetUniformLocation(shaderProgramID, "opacity");
		}
		
	    fogColorUniform = gl.glGetUniformLocation(shaderProgramID, "fogColor");
	    fogDensityUniform = gl.glGetUniformLocation(shaderProgramID, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);

		//set texture
		if(useAlpha){
			gl.glUniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		}
	}
	
	public void setOpacity(GL2ES2 gl, float opacity){
		
		//set opacity
		gl.glUniform1f(opacityUniform, opacity);
	}

	public void setFog(GL2ES2 gl, Vector3d fogColor, float fogDensity){
		
		//set fog color uniform
		gl.glUniform3f(fogColorUniform, fogColor.getX(), fogColor.getY(), fogColor.getZ());
		gl.glUniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setVertexAttributeBuffers(GL2ES2 gl, Drawable<?,GLBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		if(useAlpha){
			drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		}
	}
}
