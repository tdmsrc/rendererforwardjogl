package engine.renderer.forwardjogl.shader.lightpass;

import javax.media.opengl.GL2ES2;

import engine.renderer.Drawable;
import engine.renderer.forwardjogl.object.GLBuffer;
import geometry.math.Vector3d;


public class ShaderWireframe extends ShaderGeneric{

	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0;
	
	//wireframe color
	private int colorUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderWireframe(GL2ES2 gl){
		super(gl, "Wireframe shader");
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderWireframeVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderWireframeFrag.glsl"));
		
		finishAttaching(gl);
	}

	@Override
	protected void setVertexAttributes(GL2ES2 gl) {
		
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
	    colorUniform = gl.glGetUniformLocation(shaderProgramID, "wireframeColor");
	}

	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================

	public void setColor(GL2ES2 gl, Vector3d color){
		
		//set color uniform
		gl.glUniform3f(colorUniform, color.getX(), color.getY(), color.getZ());
	}
	
	public void setVertexAttributeBuffers(GL2ES2 gl, Drawable<?,GLBuffer> drawable){

		drawable.getWireframeBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
	}
}
