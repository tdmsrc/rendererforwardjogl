package engine.renderer.forwardjogl.shader.lightpass;

import javax.media.opengl.GL2ES2;

import engine.scene.Light;

public class ShaderLightPassDirectional extends ShaderLightPass{

	public static final int SHADOW_MAP_TEXTURE_UNIT = 0;
	
	private int textureShadowMapUniform;
	
	private float[] lightProjectionMatrixArray, lightModelViewRotationMatrixArray;
	private int lightProjectionMatrixUniform, lightModelViewRotationMatrixUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderLightPassDirectional(GL2ES2 gl, boolean normalMapping) {
		super(gl, normalMapping);
		
		lightProjectionMatrixArray = new float[16];
		lightModelViewRotationMatrixArray = new float[9];
	}
	
	@Override
	protected void attachShadowShaders(GL2ES2 gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", getClass().getResourceAsStream("/resource/shader/lightpass/IsShadowedDirectional.glsl"));
	}

	@Override
	protected void setUniforms(GL2ES2 gl) {
		super.setUniforms(gl);
		
		textureShadowMapUniform = gl.glGetUniformLocation(shaderProgramID, "texShadowMap");
		
		lightProjectionMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "lightProjectionMatrix");
		lightModelViewRotationMatrixUniform = gl.glGetUniformLocation(shaderProgramID, "lightModelViewRotation");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GL2ES2 gl){
		super.useShader(gl);
		
		gl.glUniform1i(textureShadowMapUniform, SHADOW_MAP_TEXTURE_UNIT);
	}
	
	public void setAmbientToShadowMapTransformation(GL2ES2 gl, Light light){
		
		light.getCamera().getRotation().getMatrix().serializeColumnMajor(lightModelViewRotationMatrixArray);
		gl.glUniformMatrix3fv(lightModelViewRotationMatrixUniform, 1, false, lightModelViewRotationMatrixArray, 0);
		
		light.getCamera().getProjectionMatrix().serializeColumnMajor(lightProjectionMatrixArray);
		gl.glUniformMatrix4fv(lightProjectionMatrixUniform, 1, false, lightProjectionMatrixArray, 0);
	}
}
