package engine.renderer.forwardjogl.shader;

import javax.media.opengl.GL2ES2;

import geometry.common.MessageOutput;


public class FragmentShader extends Shader{

	public FragmentShader(GL2ES2 gl, String fragmentShaderSource){
		super(gl.glCreateShader(GL2ES2.GL_FRAGMENT_SHADER));
		
		MessageOutput.printDebug("Compiling fragment shader");
		gl.glShaderSource(shaderID, 1, new String[]{ fragmentShaderSource }, null);
		gl.glCompileShader(shaderID);
		if(CHECK_ERRORS){ checkCompileStatus(gl); }
	}
}