package engine.renderer.forwardjogl.shader;

import javax.media.opengl.GL2ES2;

import geometry.common.MessageOutput;


public class VertexShader extends Shader{

	public VertexShader(GL2ES2 gl, String vertexShaderSource){
		super(gl.glCreateShader(GL2ES2.GL_VERTEX_SHADER));
		
		MessageOutput.printDebug("Compiling vertex shader");
		gl.glShaderSource(shaderID, 1, new String[]{ vertexShaderSource }, null);
		gl.glCompileShader(shaderID);
		if(CHECK_ERRORS){ checkCompileStatus(gl); }
	}
}