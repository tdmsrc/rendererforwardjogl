package engine.renderer.forwardjogl.buffer;

import javax.media.opengl.GL2ES2;

import geometry.common.MessageOutput;


public class FramebufferObject{

	private int FBOid;
	
	public FramebufferObject(GL2ES2 gl){
		
		//create FBO
		int[] createFBOIDs = new int[1];
		gl.glGenFramebuffers(createFBOIDs.length, createFBOIDs, 0);
		
		FBOid = createFBOIDs[0];
	}
	
	public void delete(GL2ES2 gl){
		
		int[] delFBOIDs = new int[]{ FBOid };
		gl.glDeleteFramebuffers(delFBOIDs.length, delFBOIDs, 0);
		
		MessageOutput.printDebug("Deleted FBO");
	}

	public void bind(GL2ES2 gl){
		gl.glBindFramebuffer(GL2ES2.GL_FRAMEBUFFER, FBOid);
	}
	
	public void unbind(GL2ES2 gl){
		gl.glBindFramebuffer(GL2ES2.GL_FRAMEBUFFER, 0);
	}

}
