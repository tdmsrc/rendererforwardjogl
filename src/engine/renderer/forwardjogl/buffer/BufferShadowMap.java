package engine.renderer.forwardjogl.buffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import geometry.common.MessageOutput;


public class BufferShadowMap{

	private int width, height;
	
	private int texidDepth;

	
	public BufferShadowMap(GL2ES2 gl, int width, int height){

		this.width = width;
		this.height = height;

		create(gl);
	}

	public int getHeight(){ return height; }
	public int getWidth(){ return width; }
	
	public void resize(GL2ES2 gl, int width, int height){

		delete(gl);
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	private void create(GL2ES2 gl){
		
		//create texture
		int[] createTexIDs = new int[1];
		gl.glGenTextures(createTexIDs.length, createTexIDs, 0);
		texidDepth = createTexIDs[0];
		
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, texidDepth);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_MIN_FILTER, GL2ES2.GL_NEAREST);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_MAG_FILTER, GL2ES2.GL_NEAREST);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_WRAP_S, GL2ES2.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_WRAP_T, GL2ES2.GL_CLAMP_TO_EDGE);
		gl.glTexImage2D(GL2ES2.GL_TEXTURE_2D, 0, GL2ES2.GL_DEPTH_COMPONENT, width, height, 0, GL2ES2.GL_DEPTH_COMPONENT, GL2ES2.GL_UNSIGNED_INT, null);
		
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, 0);
	}
	
	public void delete(GL2ES2 gl){
		
		int[] delTexIDs = new int[]{ texidDepth };
		gl.glDeleteTextures(delTexIDs.length, delTexIDs, 0);
		
		MessageOutput.printDebug("Deleted directional shadow map");
	}

	public void clearDepth(GL2ES2 gl){

		gl.glDepthMask(true); 
		gl.glClear(GL2ES2.GL_DEPTH_BUFFER_BIT);
	}
	
	public void bindDrawTo(GL2 gl){

		//attach texture
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, 
			GL2.GL_DEPTH_ATTACHMENT, GL2.GL_TEXTURE_2D, texidDepth, 0);
		
		//specify drawbuffers and readbuffers
		//[TODO] for GLES2, use GL_OES_depth_texture?
		gl.glDrawBuffer(GL2.GL_NONE);
		gl.glReadBuffer(GL2.GL_NONE);
		
		//check if it worked
		int status = gl.glCheckFramebufferStatus(GL2.GL_FRAMEBUFFER);
		if(status != GL2.GL_FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
		
		//set appropriate viewport
		gl.glViewport(0, 0, width, height);
	}
	
	public void unbindDrawTo(GL2ES2 gl){

		//detach texture
		gl.glFramebufferTexture2D(GL2ES2.GL_FRAMEBUFFER, 
			GL2ES2.GL_DEPTH_ATTACHMENT, GL2ES2.GL_TEXTURE_2D, 0, 0);
	}
	
	public void bindAsTexture(GL2ES2 gl, int texUnit){
		
		gl.glActiveTexture(GL2ES2.GL_TEXTURE0+texUnit);
	    gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, texidDepth);
	}
	
	public void unbindAsTexture(GL2ES2 gl, int texUnit){

		gl.glActiveTexture(GL2ES2.GL_TEXTURE0+texUnit);
	    gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, 0);
	}
}
