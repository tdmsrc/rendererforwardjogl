package engine.renderer.forwardjogl.buffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import geometry.common.MessageOutput;
import geometry.math.Vector4d;


public class BufferColor implements DrawBuffer{
	 
	private int width, height;
	private int texidColor;
	
	
	public BufferColor(GL2ES2 gl, int width, int height){
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	@Override
	public int getHeight(){ return height; }

	@Override
	public int getWidth(){ return width; }
	
	@Override
	public void resize(GL2ES2 gl, int width, int height){

		delete(gl);
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	private void create(GL2ES2 gl){

		//generate textures
		int[] createTexIDs = new int[1];
		gl.glGenTextures(createTexIDs.length, createTexIDs, 0);
		texidColor = createTexIDs[0];
		
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, texidColor);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_MIN_FILTER, GL2ES2.GL_LINEAR);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_MAG_FILTER, GL2ES2.GL_LINEAR);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_WRAP_S, GL2ES2.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL2ES2.GL_TEXTURE_2D, GL2ES2.GL_TEXTURE_WRAP_T, GL2ES2.GL_CLAMP_TO_EDGE);
		gl.glTexImage2D(GL2ES2.GL_TEXTURE_2D, 0, GL2ES2.GL_RGBA8, width, height, 0, GL2ES2.GL_RGBA, GL2ES2.GL_UNSIGNED_BYTE, null);
		
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, 0);
		
		MessageOutput.printDebug("Created draw buffer at " + width + "x" + height);
	}
	
	@Override
	public void delete(GL2ES2 gl){

		int[] delTexIDs = new int[]{ texidColor };
		gl.glDeleteTextures(delTexIDs.length, delTexIDs, 0);
		
		MessageOutput.printDebug("Deleted draw buffer");
	}

	@Override
	public void clearDepth(GL2ES2 gl){ }
	
	@Override
	public void clearColor(GL2ES2 gl, Vector4d color){
		
		gl.glColorMask(true, true, true, true);
		
		gl.glClearColor(color.getX(), color.getY(), color.getZ(), color.getW());
		gl.glClear(GL2ES2.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void clearDepthAndColor(GL2ES2 gl, Vector4d color){
		clearColor(gl, color);
	}
	
	@Override
	public void bindDrawTo(GL2 gl){
		
		//ensure render buffer is not attached
		gl.glFramebufferRenderbuffer(GL2.GL_FRAMEBUFFER, 
			GL2.GL_DEPTH_ATTACHMENT, GL2.GL_RENDERBUFFER, 0);
		
		//attach texture
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, 
			GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, texidColor, 0);
		
		//specify drawbuffers and readbuffers (unnecessary on GLES2)
		gl.glDrawBuffer(GL2.GL_COLOR_ATTACHMENT0);  
		gl.glReadBuffer(GL2.GL_NONE);
		
		//check if it worked
		int status = gl.glCheckFramebufferStatus(GL2.GL_FRAMEBUFFER);
		if(status != GL2.GL_FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
			
		//set appropriate viewport
		gl.glViewport(0, 0, width, height);
	}
	
	@Override
	public void unbindDrawTo(GL2ES2 gl){

		//detach texture
		gl.glFramebufferTexture2D(GL2ES2.GL_FRAMEBUFFER, 
			GL2ES2.GL_COLOR_ATTACHMENT0, GL2ES2.GL_TEXTURE_2D, 0, 0);
	}
	
	public void bindReadFrom(GL2 gl){
		
		//attach texture
		gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, 
			GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, texidColor, 0);
		
		//specify drawbuffers and readbuffers
		//TODO: not necessary on GLES2?
		gl.glDrawBuffer(GL2.GL_NONE);
		gl.glReadBuffer(GL2.GL_COLOR_ATTACHMENT0);
		
		//check if it worked
		int status = gl.glCheckFramebufferStatus(GL2.GL_FRAMEBUFFER);
		if(status != GL2.GL_FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
		
		//set appropriate viewport
		gl.glViewport(0, 0, width, height);
	}
	
	public void unbindReadFrom(GL2ES2 gl){
		
		//detach texture
		gl.glFramebufferTexture2D(GL2ES2.GL_FRAMEBUFFER, 
			GL2ES2.GL_COLOR_ATTACHMENT0, GL2ES2.GL_TEXTURE_2D, 0, 0);
	}
	
	@Override
	public void bindAsTexture(GL2ES2 gl, int texUnit){
		
		gl.glActiveTexture(GL2ES2.GL_TEXTURE0+texUnit);
	    gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, texidColor);
	}
	
	@Override
	public void unbindAsTexture(GL2ES2 gl, int texUnit){

		gl.glActiveTexture(GL2ES2.GL_TEXTURE0+texUnit);
		gl.glBindTexture(GL2ES2.GL_TEXTURE_2D, 0);
	}
}