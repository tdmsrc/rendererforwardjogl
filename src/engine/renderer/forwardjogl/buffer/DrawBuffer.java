package engine.renderer.forwardjogl.buffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import geometry.math.Vector4d;

public interface DrawBuffer{
	
	public int getWidth();
	public int getHeight();

	public void resize(GL2ES2 gl, int width, int height);
	public void delete(GL2ES2 gl);
	
	public void clearDepth(GL2ES2 gl);
	public void clearColor(GL2ES2 gl, Vector4d color);
	public void clearDepthAndColor(GL2ES2 gl, Vector4d color);
	
	public void bindDrawTo(GL2 gl);
	public void unbindDrawTo(GL2ES2 gl);
	
	public void bindAsTexture(GL2ES2 gl, int texUnit);
	public void unbindAsTexture(GL2ES2 gl, int texUnit);
}
