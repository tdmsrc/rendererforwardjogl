package engine.renderer.forwardjogl.core;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.object.GLTexture;
import engine.renderer.forwardjogl.shader.lightpass.ShaderLightPassDirectional;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainLightPassDirectional;
import engine.scene.Light;
import engine.terrain.Terrain;
import geometry.math.Camera;


public class RenderPassLightDirectional extends RenderPassLightGeneric
	<ShaderLightPassDirectional, ShaderTerrainLightPassDirectional>{


	public RenderPassLightDirectional(GL2ES2 gl){
		super(
			new ShaderLightPassDirectional(gl, false), 
			new ShaderLightPassDirectional(gl, true),
			new ShaderTerrainLightPassDirectional(gl));
	}
	
	@Override
	public void initializePass(GL2ES2 gl, Light light, Camera camera, float fogDensity){
		super.initializePass(gl, light, camera, fogDensity);
		
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GL2,GLBuffer,GLTexture> terrain){
		super.actionTerrainBegin(terrain);
		
		shaderTerrain.setAmbientToShadowMapTransformation(gl, light);
	}
	
	@Override
	protected boolean bindShader(){
		if(!super.bindShader()){ return false; }
		
		shader.setAmbientToShadowMapTransformation(gl, light);
		return true;
	}
	
	@Override
	protected boolean bindShaderBump(){
		if(!super.bindShaderBump()){ return false; }
		
		shaderBump.setAmbientToShadowMapTransformation(gl, light);
		return true;
	}
}
