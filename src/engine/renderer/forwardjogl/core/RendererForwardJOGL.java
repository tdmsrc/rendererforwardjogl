package engine.renderer.forwardjogl.core;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLCapabilitiesImmutable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

import engine.renderer.GenericBuffer.BufferConstructor;
import engine.renderer.Renderer;
import engine.renderer.forwardjogl.buffer.BufferColor;
import engine.renderer.forwardjogl.buffer.BufferColorAndDepth;
import engine.renderer.forwardjogl.buffer.BufferShadowMap;
import engine.renderer.forwardjogl.buffer.BufferShadowMapCube;
import engine.renderer.forwardjogl.buffer.DrawBuffer;
import engine.renderer.forwardjogl.buffer.FramebufferObject;
import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.object.GLTexture;
import engine.renderer.forwardjogl.shader.lightpass.ShaderLightPassCube;
import engine.renderer.forwardjogl.shader.lightpass.ShaderLightPassDirectional;
import engine.renderer.forwardjogl.shader.quad.ShaderQuad;
import engine.renderer.forwardjogl.shader.quad.ShaderQuadBlur;
import engine.renderer.forwardjogl.shader.quad.ShaderQuadBlur.BlurDirection;
import engine.renderer.forwardjogl.shader.quad.ShaderQuadBlurRadial;
import engine.renderer.forwardjogl.shader.quad.ShaderQuadColorAdjust;
import engine.renderer.forwardjogl.shader.quad.ShaderQuadCopyAlpha;
import engine.renderer.forwardjogl.shader.quad.ShaderQuadDepthOfField;
import engine.renderer.forwardjogl.shader.quad.ShaderQuadHSVAdjust;
import engine.renderer.forwardjogl.shader.skybox.ShaderSkybox;
import engine.scene.DrawOptions;
import engine.scene.Light;
import engine.scene.Skybox.SkyboxFace;
import engine.tools.Quad;
import geometry.common.MessageOutput;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.math.Vector4d;

public class RendererForwardJOGL 
	extends Renderer<GLCanvas, GL2, GLBuffer, GLTexture> 
	implements GLEventListener{

	private static final Vector4d CLEAR_COLOR_DEFAULT = new Vector4d(0.7f, 0.8f, 1.0f, 0.0f);
	private static final Vector4d CLEAR_COLOR_BLACK = new Vector4d(0.0f, 0.0f, 0.0f, 0.0f);
	
	private static final boolean NO_TRANSLUCENT_LAYER = false;
	
	//GUI component for this renderer
	private RendererGUIComponent viewer;
	private int viewportWidth, viewportHeight;
	
	//main FBO
	private FramebufferObject fbo;
	
	//single quad for displaying a buffer
	private Quad<GL2,GLBuffer> quad;
	private ShaderQuad shaderQuadCopy;
	private ShaderQuadCopyAlpha shaderQuadCopyAlpha;
	
	//buffers
	private BufferColor bufBlend; //buffer into which each pass gets blended
	private BufferColorAndDepth bufDraw; //buffer onto which each pass is drawn
	
	//post-processing (bloom, color adjust)
	private ShaderQuadBlur shaderQuadBlurH, shaderQuadBlurV;
	private static final int MINI_BUFFER_WIDTH = 512, MINI_BUFFER_HEIGHT = 512;
	private BufferColor bufMiniFront, bufMiniBack;
	
	private ShaderQuadDepthOfField shaderQuadDepthOfField;
	//private ShaderQuadSSAO shaderQuadSSAO;
	private ShaderQuadColorAdjust shaderQuadColorAdjust;
	private ShaderQuadHSVAdjust shaderQuadHSVAdjust;
	private ShaderQuadBlurRadial shaderQuadBlurRadial;
	
	//shadow map buffers and shaders
	private static final int SHADOW_MAP_WIDTH = 1024, SHADOW_MAP_HEIGHT = 1024;
	private BufferShadowMap bufShadowMap;
	private BufferShadowMapCube bufShadowMapCube;
	
	//skybox shader
	private ShaderSkybox shaderSkybox;
	
	//cameras for use with cube map
	private ArrayList<Camera> cameraCubeMap;
	
	private static final float SHADOW_MAP_CUBE_ZNEAR = 0.1f, SHADOW_MAP_CUBE_ZFAR = 1024.0f;
	private static ArrayList<Camera> makeCubeMapCameras(){
		
		//the position and z-clipping planes don't matter:
		//they will be set according to the values in the omni light's camera
		ArrayList<Camera> camList = new ArrayList<Camera>(6);
		Camera cam;

		cam = new Camera(90.0f, 1.0f, SHADOW_MAP_CUBE_ZNEAR, SHADOW_MAP_CUBE_ZFAR); 
		cam.getRotation().setForward(new Vector3d(1.0f, 0.0f, 0.0f)); 
		cam.getRotation().setFixedUp(new Vector3d(0.0f, -1.0f, 0.0f));
		camList.add(cam);
		
		cam = new Camera(90.0f, 1.0f, SHADOW_MAP_CUBE_ZNEAR, SHADOW_MAP_CUBE_ZFAR); 
		cam.getRotation().setForward(new Vector3d(-1.0f, 0.0f, 0.0f)); 
		cam.getRotation().setFixedUp(new Vector3d(0.0f, -1.0f, 0.0f));
		camList.add(cam);
		
		cam = new Camera(90.0f, 1.0f, SHADOW_MAP_CUBE_ZNEAR, SHADOW_MAP_CUBE_ZFAR); 
		cam.getRotation().setForward(new Vector3d(0.0f, 1.0f, 0.0f)); 
		cam.getRotation().setFixedUp(new Vector3d(0.0f, 0.0f, 1.0f));
		camList.add(cam);
		
		cam = new Camera(90.0f, 1.0f, SHADOW_MAP_CUBE_ZNEAR, SHADOW_MAP_CUBE_ZFAR); 
		cam.getRotation().setForward(new Vector3d(0.0f, -1.0f, 0.0f)); 
		cam.getRotation().setFixedUp(new Vector3d(0.0f, 0.0f, -1.0f));
		camList.add(cam);
		
		cam = new Camera(90.0f, 1.0f, SHADOW_MAP_CUBE_ZNEAR, SHADOW_MAP_CUBE_ZFAR); 
		cam.getRotation().setForward(new Vector3d(0.0f, 0.0f, 1.0f)); 
		cam.getRotation().setFixedUp(new Vector3d(0.0f, -1.0f, 0.0f));
		camList.add(cam);
		
		cam = new Camera(90.0f, 1.0f, SHADOW_MAP_CUBE_ZNEAR, SHADOW_MAP_CUBE_ZFAR); 
		cam.getRotation().setForward(new Vector3d(0.0f, 0.0f, -1.0f));
		cam.getRotation().setFixedUp(new Vector3d(0.0f, -1.0f, 0.0f));
		camList.add(cam);
		
		return camList;
	}

	//make RenderPass objects
	private RenderPassWireframe renderPassWireframe;
	private RenderPassZPrepass renderPassZPrepass;
	private RenderPassAmbientAndEmissive renderPassAmbientAndEmissive;
	private RenderPassLight renderPassLight;
	private RenderPassLightDirectional renderPassLightDirectional;
	private RenderPassLightCube renderPassLightCube;
	private RenderPassShadowMap renderPassShadowMap;
	
	
	public RendererForwardJOGL(int viewportWidth, int viewportHeight){
		super();
		
		this.viewportWidth = viewportWidth;
		this.viewportHeight = viewportHeight;
		
		GLProfile glp = GLProfile.get("GL2ES2");
		GLCapabilities glCapabilities = new GLCapabilities(glp);
		//glCapabilities.setDoubleBuffered(false);
		
		viewer = new RendererGUIComponent(glCapabilities);
		//viewer.setAutoSwapBufferMode(false);
		viewer.addGLEventListener(this);
		
		//System.setProperty("sun.awt.noerasebackground", "true");
		//System.setProperty("sun.java2d.noddraw", "true");
	}
	
	//==============================================
	// Renderer
	//==============================================
	
	@Override
	public void render(){ viewer.display(); }
	
	@Override
	public GLCanvas getCanvasObject(){ return viewer; }

	@Override
	public BufferConstructor<GL2,GLBuffer> getBufferConstructor(){
		return GLBuffer.getConstructor();
	}
	
	//==============================================
	// GLEventListener
	//==============================================
	
	@Override
	public void init(GLAutoDrawable drawable){
		final GL2 gl = drawable.getGL().getGL2();
		
		//drawable.setAutoSwapBufferMode(true);
		GLCapabilitiesImmutable glc = drawable.getChosenGLCapabilities();	
		//glc.setDoubleBuffered(false);
		MessageOutput.printDebug("GLCapabilities: ");
		MessageOutput.printDebug(glc.toString());
		
		MessageOutput.printDebug("GL_VENDOR: " + gl.glGetString(GL.GL_VENDOR));
	    MessageOutput.printDebug("GL_RENDERER: " + gl.glGetString(GL.GL_RENDERER));
	    MessageOutput.printDebug("GL_VERSION: " + gl.glGetString(GL.GL_VERSION));
	    MessageOutput.printDebug("GL_SHADING_LANGUAGE_VERSION: " + gl.glGetString(GL2.GL_SHADING_LANGUAGE_VERSION));
	    MessageOutput.printDebug("GL_EXTENSIONS: " + gl.glGetString(GL.GL_EXTENSIONS));
	    
	    //some other stuff
	    int[] intOut = new int[1];
	    gl.glGetIntegerv(GL2.GL_MAX_TEXTURE_SIZE, intOut, 0);
	    MessageOutput.printDebug("GL_MAX_TEXTURE_SIZE: " + intOut[0]);
	    
	    //main FBO
	    fbo = new FramebufferObject(gl);
	    
		//make quad
		quad = new Quad<GL2,GLBuffer>(gl, GLBuffer.getConstructor());
		shaderQuadCopy = new ShaderQuad(gl, "Copy quad shader");
		shaderQuadCopyAlpha = new ShaderQuadCopyAlpha(gl);
		shaderQuadDepthOfField = new ShaderQuadDepthOfField(gl);
		//shaderQuadSSAO = new ShaderQuadSSAO(gl);
		shaderQuadColorAdjust = new ShaderQuadColorAdjust(gl);
		shaderQuadHSVAdjust = new ShaderQuadHSVAdjust(gl);
		shaderQuadBlurRadial = new ShaderQuadBlurRadial(gl);
		
		//make drawbuffers
		bufDraw = new BufferColorAndDepth(gl, viewportWidth, viewportHeight);
		bufBlend = new BufferColor(gl, viewportWidth, viewportHeight);
		
		//bloom stuff
		shaderQuadBlurH = new ShaderQuadBlur(gl, BlurDirection.BLUR_DIRECTION_HORIZONTAL);
		shaderQuadBlurV = new ShaderQuadBlur(gl, BlurDirection.BLUR_DIRECTION_VERTICAL);
		bufMiniFront = new BufferColor(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
		bufMiniBack = new BufferColor(gl, MINI_BUFFER_WIDTH, MINI_BUFFER_HEIGHT);
		
	    //make shadow maps and shader
		bufShadowMap = new BufferShadowMap(gl, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
		bufShadowMapCube = new BufferShadowMapCube(gl, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
		
		//make skybox shader
		shaderSkybox = new ShaderSkybox(gl);
		
		//make cube map cameras
		cameraCubeMap = makeCubeMapCameras();
		
		//make render passes
		renderPassWireframe = new RenderPassWireframe(gl);
		renderPassZPrepass = new RenderPassZPrepass(gl);
		renderPassAmbientAndEmissive = new RenderPassAmbientAndEmissive(gl);
		renderPassLight = new RenderPassLight(gl);
		renderPassLightDirectional = new RenderPassLightDirectional(gl);
		renderPassLightCube = new RenderPassLightCube(gl);
		renderPassShadowMap = new RenderPassShadowMap(gl);
		
		//defaults
		gl.glEnable(GL2ES2.GL_CULL_FACE);
		gl.glCullFace(GL2ES2.GL_BACK);
		
		fbo.bind(gl);
		
		//notify initialize listeners
	    notifyInitializeListeners(gl);
	    
	    fbo.unbind(gl);
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height){
        final GL2 gl = drawable.getGL().getGL2();
        
        this.viewportWidth = width;
        this.viewportHeight = height;
        
        //resize buffers
        bufDraw.resize(gl, width, height);
        bufBlend.resize(gl, width, height);
        
        //notify resize listeners
        notifyResizeListeners(width, height);
        
        //set gl viewport dimensions
        gl.glViewport(x, y, width, height);
	}
	
	@Override
	public void dispose(GLAutoDrawable drawable){
		final GL2 gl = drawable.getGL().getGL2();
		
	    //notify dispose listeners
	    notifyDisposeListeners(gl);
		
		//delete FBO
		fbo.delete(gl);
		
		//delete buffers
		bufBlend.delete(gl);
		bufDraw.delete(gl);
		
		bufMiniFront.delete(gl);
		bufMiniBack.delete(gl);
		
		//delete shadow maps
		bufShadowMap.delete(gl);
		bufShadowMapCube.delete(gl);
	}

	@Override
	public void display(GLAutoDrawable drawable){
		final GL2 gl = drawable.getGL().getGL2();
		
		//bind FBO
		fbo.bind(gl);
		
		//notify display listeners
		notifyDisplayListeners(gl);
		
		//check for null scene or camera 
		if((scene == null) || (camera == null)){
			if(scene == null){ MessageOutput.printWarning("(Renderer.display) Null scene"); }
			if(camera == null){ MessageOutput.printWarning("(Renderer.display) Null camera"); }
			fbo.unbind(gl); return;
		}
		
		//solid geometry and skybox
		bufDraw.bindDrawTo(gl);
		bufDraw.clearDepthAndColor(gl, CLEAR_COLOR_DEFAULT);
		renderZPrepass(gl, false);
		renderAmbientAndEmissive(gl, false);
		if(scene.hasSkybox()){ renderSkybox(gl); }
		bufDraw.unbindDrawTo(gl);
		
		renderLighting(gl, false); //<- handles binding and unbinding bufDraw
		
		//blend bufDraw into bufBlend, using DoF if enabled
		if(scene.getFX().getDepthOfFieldEnabled()){ 
			prepareDepthOfField(gl, false, bufDraw); 
			
			gl.glEnable(GL2.GL_BLEND);
			gl.glBlendFunc(GL2.GL_ONE, GL2.GL_ONE);
			
			displayDepthOfField(gl, bufDraw, bufBlend, true);
		}else{
			//gl.glDisable(GL2.GL_BLEND); //bufBlend has no alpha channel
			//gl.glDisable(GL2.GL_DEPTH_TEST); //bufBlend has no depth buffer
			bufBlend.bindDrawTo(gl);
			bufBlend.clearColor(gl, CLEAR_COLOR_BLACK);
			drawBufferCopy(gl, bufDraw);
			bufBlend.unbindDrawTo(gl);
		}
		
		//light scattering, if enabled
		if(scene.hasSkybox() && scene.getSkybox().hasSun()){
			renderLightScattering(gl, bufBlend);	
		}
		
		if(!NO_TRANSLUCENT_LAYER){
			
			//translucent geometry
			bufDraw.bindDrawTo(gl);
			
			//bufDraw.clearColor(gl, CLEAR_COLOR_BLACK); //set color to vec4(0,0,0,0) but preserve zbuffer 
			gl.glColorMask(false, false, false, true); //set only alpha to 0
			gl.glClearColor(0,0,0,0);
			gl.glClear(GL2ES2.GL_COLOR_BUFFER_BIT);
			
			renderZPrepass(gl, true);
			renderAmbientAndEmissive(gl, true);
			bufDraw.unbindDrawTo(gl);
			
			renderLighting(gl, true); //<- handles binding and unbinding bufDraw
			
			//blend bufDraw into bufBlend, using DoF if enabled
			if(scene.getFX().getDepthOfFieldEnabled()){ 
				prepareDepthOfField(gl, true, bufDraw); 
				
				gl.glEnable(GL2.GL_BLEND);
				gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
				
				displayDepthOfField(gl, bufDraw, bufBlend, false);
			}else{
				gl.glEnable(GL2.GL_BLEND);
				gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
				
				bufBlend.bindDrawTo(gl);
				drawBufferCopy(gl, bufDraw);
				bufBlend.unbindDrawTo(gl);
			}
		}
		//TODO: SSAO (if enabled): bufDraw should have full depth info
		/*prepareSSAO(gl); 
		
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		
		displaySSAO(gl, bufBlend, false);*/
		
		//bloom (if enabled)
		if(scene.getFX().getBlurEnabled()){ displayBloom(gl); }
		
		//wireframe (after bloom to avoid aliasing artifacts from downsampling)
		renderWireframe(gl);
		
		//display final image (using color adjust shader if enabled)
		fbo.unbind(gl);
		
		gl.glDisable(GL2.GL_BLEND);
		gl.glDisable(GL2.GL_DEPTH_TEST);
		
		if(scene.getFX().getColorAdjustEnabled()){ drawBufferColorAdjust(gl, bufBlend); }
		else if(scene.getFX().getHSVAdjustEnabled()){ drawBufferHSVAdjust(gl, bufBlend); }
		else{ drawBufferCopy(gl, bufBlend); }
	}
	
	//==============================================
	// QUAD RENDERING
	//==============================================
	
	private void drawBufferInitialize(GL2 gl, DrawBuffer buffer, ShaderQuad shader){
		
		shader.useShader(gl);
		shader.setVertexAttributeBuffers(gl, quad);
		buffer.bindAsTexture(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
	}
	
	private void drawBufferFinish(GL2 gl, DrawBuffer buffer, ShaderQuad shader){
		
		gl.glDrawArrays(GL2.GL_TRIANGLES, 0, 3*quad.getTriCount());
		buffer.unbindAsTexture(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		shader.unuseShader(gl);
	}
	
	private void drawBufferCopy(GL2 gl, DrawBuffer buffer){
		
		drawBufferInitialize(gl, buffer, shaderQuadCopy);
		drawBufferFinish(gl, buffer, shaderQuadCopy);
	}
	
	private void drawBufferAlpha(GL2 gl, DrawBuffer buffer, float alpha){
		
		drawBufferInitialize(gl, buffer, shaderQuadCopyAlpha);
		shaderQuadCopyAlpha.setAlpha(gl, alpha);
		drawBufferFinish(gl, buffer, shaderQuadCopyAlpha);
	}
	
	private void drawBufferColorAdjust(GL2 gl, DrawBuffer buffer){
		
		drawBufferInitialize(gl, buffer, shaderQuadColorAdjust);
		shaderQuadColorAdjust.setColorAdjust(gl, 
			scene.getFX().getColorAdjustBrightness(), 
			scene.getFX().getColorAdjustContrast(), 
			scene.getFX().getColorAdjustSaturation());
		drawBufferFinish(gl, buffer, shaderQuadColorAdjust);
	}
	
	private void drawBufferHSVAdjust(GL2 gl, DrawBuffer buffer){
		
		drawBufferInitialize(gl, buffer, shaderQuadHSVAdjust);
		shaderQuadHSVAdjust.setHSVAdjust(gl, 
			scene.getFX().getHSVAdjustHue(), 
			scene.getFX().getHSVAdjustSaturation(), 
			scene.getFX().getHSVAdjustValue());
		drawBufferFinish(gl, buffer, shaderQuadHSVAdjust);
	}
	
	/*private void drawShadowMap(GL2 gl, ShaderQuad shader){
		
		shader.useShader(gl);
		shader.setTextureSize(gl, bufShadowMap.getWidth(), bufShadowMap.getHeight());
		shader.setVertexAttributeBuffers(gl, quad);
		bufShadowMap.bindAsTexture(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		quad.draw(gl);
		bufShadowMap.unbindAsTexture(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		shader.unuseShader(gl);
	}*/
	
	//will blur the buffer "buffer"; result will be in bufMiniBack 
	private void performBlur(GL2 gl, DrawBuffer buffer){
		
		bufMiniBack.bindDrawTo(gl);
		bufMiniBack.clearDepth(gl);
		bufMiniBack.clearColor(gl, CLEAR_COLOR_BLACK);
		drawBufferCopy(gl, buffer);
		bufMiniBack.unbindDrawTo(gl);
		
		//copy bufMiniBack to bufMiniFront using blur H
		bufMiniFront.bindDrawTo(gl);
		bufMiniFront.clearDepth(gl);
		bufMiniFront.clearColor(gl, CLEAR_COLOR_BLACK);
		drawBufferInitialize(gl, bufMiniBack, shaderQuadBlurH);
		shaderQuadBlurH.setTextureSize(gl, bufMiniBack.getWidth(), bufMiniBack.getHeight());
		drawBufferFinish(gl, bufMiniBack, shaderQuadBlurH);
		bufMiniFront.unbindDrawTo(gl);
		
		//copy bufMiniFront to bufMiniBack using blur V
		bufMiniBack.bindDrawTo(gl);
		drawBufferInitialize(gl, bufMiniFront, shaderQuadBlurV);
		shaderQuadBlurV.setTextureSize(gl, bufMiniFront.getWidth(), bufMiniFront.getHeight());
		drawBufferFinish(gl, bufMiniFront, shaderQuadBlurV);
		bufMiniBack.unbindDrawTo(gl);
	}
	
	private void displayBloom(GL2 gl){
		
		//copy bufBlend to bufMiniBack
		gl.glDisable(GL2.GL_BLEND);
		gl.glDisable(GL2.GL_DEPTH_TEST);
		
		performBlur(gl, bufBlend);
		
		//blend bufDrawMiniFront into bufBlend
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		
		bufBlend.bindDrawTo(gl);
		drawBufferAlpha(gl, bufMiniBack, scene.getFX().getBlurOpacity());
		bufBlend.unbindDrawTo(gl);
	}
	
	//==============================================
	// DEPTH OF FIELD
	//==============================================
	
	private static final int DRAW_OPTIONS_DEPTH_FIELD = DrawOptions.MASK_SOLID_VISIBLE;
	
	private void prepareDepthOfField(GL2 gl, boolean translucent, DrawBuffer blurSource){
		
		//blur the blurSource into bufMiniBack
		gl.glDisable(GL2.GL_BLEND);
		gl.glDisable(GL2.GL_DEPTH_TEST);
		
		performBlur(gl, blurSource);
		
		//render shadow map from the actual camera perspective
		bufShadowMap.bindDrawTo(gl);
		bufShadowMap.clearDepth(gl);
		
		renderPassShadowMap.initializePass(gl, camera);
		scene.cameraTraversal(renderPassShadowMap, camera, 
			DRAW_OPTIONS_DEPTH_FIELD | DrawOptions.MASK_TRANSLUCENT, 
			DRAW_OPTIONS_DEPTH_FIELD | (translucent ? DrawOptions.MASK_TRANSLUCENT : 0));
		renderPassShadowMap.finishPass();
		
		bufShadowMap.unbindDrawTo(gl);
		
		//undo depth test, which gets enabled by renderPassShadowMap
		gl.glDisable(GL2ES2.GL_DEPTH_TEST);
	}
	
	private void displayDepthOfField(GL2 gl, DrawBuffer blurSource, BufferColor blendTarget, boolean clearTarget){
		
		//only drawing a quad onto a blend layer, so need not write to depth or alpha 
		gl.glDepthMask(false); 
		gl.glColorMask(true, true, true, false);
		
		//draw the blurSource quad combined appropriately with the textures prepared by prepareDepthOfField
		blendTarget.bindDrawTo(gl);
		
		if(clearTarget){ blendTarget.clearColor(gl, CLEAR_COLOR_BLACK); }
		
		bufShadowMap.bindAsTexture(gl, ShaderQuadDepthOfField.DEPTH_TEXTURE_UNIT);
		bufMiniBack.bindAsTexture(gl, ShaderQuadDepthOfField.BLUR_TEXTURE_UNIT);
		
		drawBufferInitialize(gl, blurSource, shaderQuadDepthOfField);
		shaderQuadDepthOfField.setParameters(gl, camera.getZNear(), camera.getZFar(), 
			scene.getFX().getDepthOfFieldFocusDistance(), scene.getFX().getDepthOfFieldFocusRange());
		drawBufferFinish(gl, blurSource, shaderQuadDepthOfField);
		
		bufShadowMap.unbindAsTexture(gl, ShaderQuadDepthOfField.DEPTH_TEXTURE_UNIT);
		bufMiniBack.unbindAsTexture(gl, ShaderQuadDepthOfField.BLUR_TEXTURE_UNIT);
		
		blendTarget.unbindDrawTo(gl);
	}
	
	//==============================================
	// SSAO
	//==============================================
	
	/*private void prepareSSAO(GL2 gl){
		
		//render shadow map from the actual camera perspective
		bufShadowMap.bindDrawTo(gl);
		bufShadowMap.clearDepth(gl);
		
		renderPassShadowMap.initializePass(gl, camera);
		scene.cameraTraversal(renderPassShadowMap, camera, 0, 0);
		renderPassShadowMap.finishPass();
		
		bufShadowMap.unbindDrawTo(gl);
		
		//undo depth test, which gets enabled by renderPassShadowMap
		gl.glDisable(GL2ES2.GL_DEPTH_TEST);
	}
	
	private void displaySSAO(GL2 gl, BufferColor blendTarget, boolean clearTarget){
		
		//only drawing a quad onto a blend layer, so need not write to depth or alpha 
		gl.glDepthMask(false); 
		gl.glColorMask(true, true, true, false);
		
		//draw bufShadowMap onto blendTarget using SSAO shader
		blendTarget.bindDrawTo(gl);
		
		if(clearTarget){ blendTarget.clearColor(gl, CLEAR_COLOR_BLACK); }
		
		shaderQuadSSAO.useShader(gl);
		shaderQuadSSAO.setVertexAttributeBuffers(gl, quad);
		bufShadowMap.bindAsTexture(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		
		shaderQuadSSAO.setParameters(gl, camera.getZNear(), camera.getZFar());
		
		gl.glDrawArrays(GL2.GL_TRIANGLES, 0, 3*quad.getTriCount());
		bufShadowMap.unbindAsTexture(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		shaderQuadSSAO.unuseShader(gl);
		
		blendTarget.unbindDrawTo(gl);
	}*/
	
	//==============================================
	// LIGHT SCATTERING
	//==============================================
	
	private void renderLightScattering(GL2 gl, DrawBuffer targetBuffer){
		
		bufMiniFront.bindDrawTo(gl);
		bufMiniFront.clearDepth(gl);
		bufMiniFront.clearColor(gl, CLEAR_COLOR_BLACK);
		
		//draw the "light blob" quad at infinite distance (i.e. using skybox shader)
		gl.glDisable(GL2ES2.GL_DEPTH_TEST);
		gl.glDepthMask(false);
		gl.glColorMask(true, true, true, false);
		gl.glDisable(GL2ES2.GL_BLEND);
		
		shaderSkybox.useShader(gl);
		
		shaderSkybox.setViewMatrices(gl, camera);
		shaderSkybox.setVertexAttributeBuffersSun(gl, scene.getSkybox());
		
		scene.getSkybox().getSunTexture().bind(gl, ShaderSkybox.SKYBOX_TEXTURE_UNIT);
		gl.glDrawArrays(GL2.GL_TRIANGLES, 0, 3*scene.getSkybox().getTriCount());
		scene.getSkybox().getSunTexture().unbind(gl, ShaderSkybox.SKYBOX_TEXTURE_UNIT);
		
		shaderSkybox.unuseShader(gl);
		
		//mask the blob by drawing opaque objects in black (shadow map shader, but override color mask settings)
		renderPassShadowMap.initializePass(gl, camera);
		gl.glColorMask(true, true, true, false);
		scene.cameraTraversal(renderPassShadowMap, camera, 
			DrawOptions.MASK_TRANSLUCENT, 
			0);
		renderPassShadowMap.finishPass();
		
		bufMiniFront.unbindDrawTo(gl);
		
		//find blur center (TODO: ideally, camera would have a getScreenCoordsFromRay)
		Vector3d sunObject = scene.getSkybox().getSunDirection().copy();
		sunObject.normalize();
		sunObject.scale(5000.0f);
		sunObject.add(camera.getPosition());
		Vector2d sunScreen = camera.getScreenCoordsFromObjectCoords(1.0f, 1.0f, false, sunObject);
		
		//copy bufMiniFront to bufMiniBack using radial blur
		bufMiniBack.bindDrawTo(gl);
		bufMiniBack.clearDepth(gl);
		bufMiniBack.clearColor(gl, CLEAR_COLOR_BLACK);
		drawBufferInitialize(gl, bufMiniFront, shaderQuadBlurRadial);
		shaderQuadBlurRadial.setBlurCenter(gl, sunScreen);
		drawBufferFinish(gl, bufMiniFront, shaderQuadBlurRadial);
		bufMiniBack.unbindDrawTo(gl);
		
		//additively blend bufMiniBack onto bufBlend
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_ONE, GL2.GL_ONE);
		
		targetBuffer.bindDrawTo(gl);
		drawBufferAlpha(gl, bufMiniBack, 1.0f);
		targetBuffer.unbindDrawTo(gl);
	}
	
	//==============================================
	// SKYBOX RENDERING
	//==============================================
	
	private void renderSkybox(GL2 gl){
		
		gl.glEnable(GL2ES2.GL_DEPTH_TEST);
		gl.glDepthFunc(GL2ES2.GL_LEQUAL);
		
		gl.glDepthMask(false);
		gl.glColorMask(true, true, true, false);
		
		gl.glDisable(GL2ES2.GL_BLEND);
		
		//render each face
		shaderSkybox.useShader(gl);
		shaderSkybox.setViewMatrices(gl, camera);
		for(SkyboxFace face : SkyboxFace.values()){
			shaderSkybox.setVertexAttributeBuffers(gl, scene.getSkybox(), face);
			
			scene.getSkybox().getTexture(face).bind(gl, ShaderSkybox.SKYBOX_TEXTURE_UNIT);
			gl.glDrawArrays(GL2.GL_TRIANGLES, 0, 3*scene.getSkybox().getTriCount());
			scene.getSkybox().getTexture(face).unbind(gl, ShaderSkybox.SKYBOX_TEXTURE_UNIT);
		}
		shaderSkybox.unuseShader(gl);
	}
	
	//==============================================
	// SHADOW MAP RENDERING
	//==============================================
	
	private static final int DRAW_OPTIONS_SHADOW_MAP = DrawOptions.MASK_CASTS_SHADOWS;
	
	private void renderShadowMapDirectional(GL2 gl, Light light, BufferShadowMap shadowMap){
		
		//draw shadow map
		shadowMap.bindDrawTo(gl);
		shadowMap.clearDepth(gl);
		
		renderPassShadowMap.initializePass(gl, light.getCamera());
		scene.cameraTraversal(renderPassShadowMap, light.getCamera(), 
			DRAW_OPTIONS_SHADOW_MAP, 
			DRAW_OPTIONS_SHADOW_MAP);
		renderPassShadowMap.finishPass();
		
		shadowMap.unbindDrawTo(gl);
	}
	
	private void renderShadowMapCube(GL2 gl, Light light, BufferShadowMapCube shadowMap){
		
		//draw shadow map
		for(int i=0; i<6; i++){
			//use cubemap cameras, but set position and zclipping planes to match light.getCamera()
			Camera cam = cameraCubeMap.get(i);
			cam.setZClippingPlanes(light.getCamera().getZNear(), light.getCamera().getZFar());
			cam.setPosition(light.getCamera().getPosition());
			
			shadowMap.bindDrawTo(gl, i);
			shadowMap.clearDepth(gl);
			
			renderPassShadowMap.initializePass(gl, cam);
			scene.cameraTraversal(renderPassShadowMap, cam, 
				DRAW_OPTIONS_SHADOW_MAP, 
				DRAW_OPTIONS_SHADOW_MAP);
			renderPassShadowMap.finishPass();
			
			shadowMap.unbindDrawTo(gl, i);
		}
	}
	
	//==============================================
	// LIGHT PASS RENDERING
	//==============================================
	
	private static final int DRAW_OPTIONS_LIGHT_PASS = 
		DrawOptions.MASK_SOLID_VISIBLE | DrawOptions.MASK_LIGHTING;
	
	private void renderLighting(GL2 gl, boolean translucent){
		
		List<Light> lights = scene.getLights(camera);
		
		for(Light light : lights){
			switch(light.getShadowType()){
			case SHADOW_NONE: 
				lightPass(gl, light, translucent); break;
			case SHADOW_DIRECTIONAL: 
				lightPassDirectional(gl, light, translucent); break;
			case SHADOW_OMNI: 
				lightPassCube(gl, light, translucent); break;
			}
		}
	}
	
	private void lightPass(GL2 gl, Light light, boolean translucent){
		
		//draw light pass
		bufDraw.bindDrawTo(gl);
		
		renderPassLight.initializePass(gl, light, camera, scene.getFX().getFogDensity());
		scene.cameraLightPassTraversal(renderPassLight, camera, light,
			DRAW_OPTIONS_LIGHT_PASS | DrawOptions.MASK_TRANSLUCENT,
			DRAW_OPTIONS_LIGHT_PASS | (translucent ? DrawOptions.MASK_TRANSLUCENT : 0));
		renderPassLight.finishPass();
		
		bufDraw.unbindDrawTo(gl);
	}
	
	private void lightPassDirectional(GL2 gl, Light light, boolean translucent){
		
		//draw shadow map
		renderShadowMapDirectional(gl, light, bufShadowMap);
		
		//draw light pass 
		bufDraw.bindDrawTo(gl);
		//assumes ShaderLightPassDirectional.SHADOW_MAP_TEXTURE_UNIT == ShaderTerrainLightPassDirectional.SHADOW_MAP_TEXTURE_UNIT
		bufShadowMap.bindAsTexture(gl, ShaderLightPassDirectional.SHADOW_MAP_TEXTURE_UNIT);
		
		renderPassLightDirectional.initializePass(gl, light, camera, scene.getFX().getFogDensity());
		scene.cameraLightPassTraversal(renderPassLightDirectional, camera, light,
			DRAW_OPTIONS_LIGHT_PASS | DrawOptions.MASK_TRANSLUCENT,
			DRAW_OPTIONS_LIGHT_PASS | (translucent ? DrawOptions.MASK_TRANSLUCENT : 0));
		renderPassLightDirectional.finishPass();
		
		bufShadowMap.unbindAsTexture(gl, ShaderLightPassDirectional.SHADOW_MAP_TEXTURE_UNIT);
		bufDraw.unbindDrawTo(gl);
	}
	
	private void lightPassCube(GL2 gl, Light light, boolean translucent){
		
		//draw shadow map
		renderShadowMapCube(gl, light, bufShadowMapCube);
		
		//draw light pass
		bufDraw.bindDrawTo(gl);
		//assumes ShaderLightPassCube.SHADOW_MAP_TEXTURE_UNIT == ShaderTerrainLightPassCube.SHADOW_MAP_TEXTURE_UNIT
		bufShadowMapCube.bindAsTexture(gl, ShaderLightPassCube.SHADOW_MAP_TEXTURE_UNIT);
		
		renderPassLightCube.initializePass(gl, light, camera, scene.getFX().getFogDensity());
		scene.cameraLightPassTraversal(renderPassLightCube, camera, light,
			DRAW_OPTIONS_LIGHT_PASS | DrawOptions.MASK_TRANSLUCENT,
			DRAW_OPTIONS_LIGHT_PASS | (translucent ? DrawOptions.MASK_TRANSLUCENT : 0));
		renderPassLightCube.finishPass();
		
		bufShadowMapCube.unbindAsTexture(gl, ShaderLightPassCube.SHADOW_MAP_TEXTURE_UNIT);
		bufDraw.unbindDrawTo(gl);
	}
	
	//==============================================
	// OTHER RENDER PASSES
	//==============================================
	
	private void renderAmbientAndEmissive(GL2 gl, boolean translucent){
		
		//if there is ambient lighting, render everything
		//if no ambient lighting, then only render emissive things
		int drawOptionsAmbientEmissive = scene.getFX().hasAmbientLight() ?
			DrawOptions.MASK_SOLID_VISIBLE :
			DrawOptions.MASK_SOLID_VISIBLE | DrawOptions.MASK_EMISSIVE;

		renderPassAmbientAndEmissive.initializePass(gl, camera, 
			scene.getFX().getAmbientColor(), 
			scene.getFX().hasAmbientLight() ? scene.getFX().getAmbientCoefficient() : 0.0f, 
			scene.getFX().getFogDensity());
		scene.cameraTraversal(renderPassAmbientAndEmissive, camera, 
			drawOptionsAmbientEmissive | DrawOptions.MASK_TRANSLUCENT,
			drawOptionsAmbientEmissive | (translucent ? DrawOptions.MASK_TRANSLUCENT : 0));
		renderPassAmbientAndEmissive.finishPass();
	}
	
	
	private static final int DRAW_OPTIONS_Z_PREPASS = DrawOptions.MASK_SOLID_VISIBLE;
	
	private void renderZPrepass(GL2 gl, boolean translucent){

		renderPassZPrepass.initializePass(gl, camera, scene.getFX().getFogDensity(), scene.getFX().getFogColor(), translucent);
		scene.cameraTraversal(renderPassZPrepass, camera, 
			DRAW_OPTIONS_Z_PREPASS | DrawOptions.MASK_TRANSLUCENT,
			DRAW_OPTIONS_Z_PREPASS | (translucent ? DrawOptions.MASK_TRANSLUCENT : 0));
		renderPassZPrepass.finishPass();
	}

	
	private static final int DRAW_OPTIONS_WIREFRAME = DrawOptions.MASK_WIREFRAME_VISIBLE;
	
	private void renderWireframe(GL2 gl){
		
		//bind draw buffer for drawing to
		bufBlend.bindDrawTo(gl);
		
		renderPassWireframe.initializePass(gl, camera);
		scene.cameraTraversal(renderPassWireframe, camera, 
			DRAW_OPTIONS_WIREFRAME, 
			DRAW_OPTIONS_WIREFRAME);
		renderPassWireframe.finishPass();
		
		//unbind buffer
		bufBlend.unbindDrawTo(gl);
	}
}
