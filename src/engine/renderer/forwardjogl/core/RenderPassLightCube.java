package engine.renderer.forwardjogl.core;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.object.GLTexture;
import engine.renderer.forwardjogl.shader.lightpass.ShaderLightPassCube;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainLightPassCube;
import engine.scene.Light;
import engine.terrain.Terrain;
import geometry.math.Camera;

public class RenderPassLightCube extends RenderPassLightGeneric
	<ShaderLightPassCube, ShaderTerrainLightPassCube>{

	private float zNear, zFar;
	
	
	public RenderPassLightCube(GL2ES2 gl){
		super(
			new ShaderLightPassCube(gl, false), 
			new ShaderLightPassCube(gl, true),
			new ShaderTerrainLightPassCube(gl));
	}

	@Override
	public void initializePass(GL2ES2 gl, Light light, Camera camera, float fogDensity){
		super.initializePass(gl, light, camera, fogDensity);

		//pull zNear and zFar from light.getCamera()
		this.zNear = light.getCamera().getZNear();
		this.zFar = light.getCamera().getZFar();
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GL2,GLBuffer,GLTexture> terrain){
		super.actionTerrainBegin(terrain);
		
		shaderTerrain.setCubeProjZBounds(gl, zNear, zFar);
	}

	@Override
	protected boolean bindShader(){
		if(!super.bindShader()){ return false; }
		
		shader.setCubeProjZBounds(gl, zNear, zFar);
		return true;
	}
	
	@Override
	protected boolean bindShaderBump(){
		if(!super.bindShaderBump()){ return false; }
		
		shaderBump.setCubeProjZBounds(gl, zNear, zFar);
		return true;
	}
}
