package engine.renderer.forwardjogl.core;

import javax.media.opengl.GLCapabilitiesImmutable;
import javax.media.opengl.awt.GLCanvas;

public class RendererGUIComponent extends GLCanvas{
	private static final long serialVersionUID = 1L;

	public RendererGUIComponent(GLCapabilitiesImmutable glCapabilities){
		super(glCapabilities);
	}	
}
