package engine.renderer.forwardjogl.core;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import engine.renderer.DrawableIBO;
import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.object.GLTexture;
import engine.renderer.forwardjogl.shader.ShaderProgram;
import engine.renderer.forwardjogl.shader.lightpass.ShaderWireframe;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainGeneric;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainWireframe;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;


public class RenderPassWireframe 
	extends SceneTraversal<GL2,GLBuffer,GLTexture>{
	
	private ShaderWireframe shader;
	private ShaderTerrainWireframe shaderTerrain;
	
	//transient data for a single pass
	protected GL2ES2 gl;
	protected Camera camera;
	
	//things that can change; store last so not flipped every object
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassWireframe(GL2ES2 gl){
		shader = new ShaderWireframe(gl);
		shaderTerrain = new ShaderTerrainWireframe(gl);
	}

	public void initializePass(GL2ES2 gl, Camera camera){
		this.gl = gl;
		this.camera = camera;
		
		//no active shader
		lastActiveObjectShader = null;
		
		setupWireframePass();
	}
	
	private void setupWireframePass(){
		
		gl.glDisable(GL2ES2.GL_DEPTH_TEST);
		
		gl.glDepthMask(false);
		gl.glColorMask(true, true, true, false);
		
		gl.glEnable(GL2ES2.GL_BLEND);
		gl.glBlendFunc(GL2ES2.GL_SRC_ALPHA, GL2ES2.GL_ONE_MINUS_SRC_ALPHA);
	}
	
	@Override
	public void actionObject(SceneObject<GL2,GLBuffer,GLTexture> sceneObject){
		if(!sceneObject.getDrawable().hasWireframeData()){ return; }
		
		//bind shader, if necessary
		bindObjectShader();
				
		shader.setColor(gl, sceneObject.getDrawOptions().getWireframeColor());
		shader.setObjectTransformation(gl, sceneObject);
		shader.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		gl.glDrawArrays(GL2ES2.GL_LINES, 0, 2*sceneObject.getDrawable().getEdgeCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GL2,GLBuffer,GLTexture> terrain){
		
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		shaderTerrain.setColor(gl, terrain.getDrawOptions().getWireframeColor());

		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<GL2,GLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		if(!tileIBO.hasWireframeData()){ return; }
		
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getWireframeIndexBuffer().bindAsElementArray(gl);
		
		gl.glDrawElements(GL2ES2.GL_LINES, 2*tileIBO.getEdgeCount(), GL2ES2.GL_UNSIGNED_SHORT, 0);
	}

	@Override
	public void actionTerrainEnd(Terrain<GL2,GLBuffer,GLTexture> terrain) {
		
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
	}
	
	protected boolean bindObjectShader(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shader){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shader.useShader(gl);
		shader.setViewMatrices(gl, camera);
		
		lastActiveObjectShader = shader;
		return true;
	}
	
	public void finishPass(){
		
		//unbind shader, if there is one bound
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
	}
}
