package engine.renderer.forwardjogl.core;

import javax.media.opengl.GL2ES2;

import engine.renderer.forwardjogl.shader.lightpass.ShaderLightPass;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainLightPass;


public class RenderPassLight extends RenderPassLightGeneric
	<ShaderLightPass, ShaderTerrainLightPass>{

	public RenderPassLight(GL2ES2 gl){
		super(
			new ShaderLightPass(gl, false), 
			new ShaderLightPass(gl, true),
			new ShaderTerrainLightPass(gl));
	}
}
