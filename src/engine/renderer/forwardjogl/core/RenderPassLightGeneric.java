package engine.renderer.forwardjogl.core;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import engine.renderer.DrawableIBO;
import engine.renderer.Material;
import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.object.GLTexture;
import engine.renderer.forwardjogl.shader.ShaderProgram;
import engine.renderer.forwardjogl.shader.lightpass.ShaderLightPass;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainGeneric;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainLightPass;
import engine.scene.Light;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;


public class RenderPassLightGeneric
	<ShaderObject extends ShaderLightPass,
	 ShaderTerrain extends ShaderTerrainLightPass>
	extends SceneTraversal<GL2,GLBuffer,GLTexture>{
	
	private static final boolean CHECK_VALIDATION = false; //[TODO] for debugging
	
	protected ShaderObject shader;
	protected ShaderObject shaderBump;
	protected ShaderTerrain shaderTerrain;
	
	//transient data for a single pass
	protected GL2ES2 gl;
	protected Light light;
	protected Camera camera;
	protected float fogDensity;
	
	//things that can change; store last so not flipped every object
	protected GLTexture lastActiveTexColor, lastActiveTexBump;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassLightGeneric(ShaderObject shader, ShaderObject shaderBump, ShaderTerrain shaderTerrain){
		
		this.shader = shader;
		this.shaderBump = shaderBump;
		this.shaderTerrain = shaderTerrain;
	}
	
	public void initializePass(GL2ES2 gl, Light light, Camera camera, float fogDensity){
		this.gl = gl;
		this.light = light;
		this.camera = camera;
		this.fogDensity = fogDensity;
		
		//no active shader or texture
		lastActiveObjectShader = null;
		lastActiveTexColor = null;
		lastActiveTexBump = null;
		
		setupLightPass();
	}
	
	private void setupLightPass(){
		
		gl.glEnable(GL2ES2.GL_DEPTH_TEST);
		gl.glDepthFunc(GL2ES2.GL_LEQUAL);
		
		gl.glDepthMask(false); 
		gl.glColorMask(true, true, true, false);
		
		gl.glEnable(GL2ES2.GL_BLEND);
		gl.glBlendFunc(GL2ES2.GL_ONE, GL2ES2.GL_ONE);
		
		//gl.glEnable(GL2ES2.GL_CULL_FACE);
		//gl.glCullFace(GL2ES2.GL_BACK);
	}
	
	@Override
	public void actionObject(SceneObject<GL2,GLBuffer,GLTexture> sceneObject){

		Material<GLTexture> mtl = sceneObject.getMaterial();
		
		//bind appropriate shader and texture, if necessary, then draw
		if(mtl.hasTexBump() && sceneObject.getDrawable().hasTangentData()){
			bindShaderBump();
			bindTexColor(mtl.getTexColor());
			
			bindTexBump(mtl.getTexBump());
			shaderBump.setReliefMappingHeight(gl, mtl.getReliefMappingHeight());
			
			shaderBump.setMaterialLightingCoefficients(gl, mtl);
			shaderBump.setObjectTransformation(gl, sceneObject);
			shaderBump.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindShader();
			bindTexColor(mtl.getTexColor());
			
			shader.setMaterialLightingCoefficients(gl, mtl);
			shader.setObjectTransformation(gl, sceneObject);
			shader.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}
		
		if(CHECK_VALIDATION){ lastActiveObjectShader.checkValidateStatus(gl); } //[TODO]
		gl.glDrawArrays(GL2ES2.GL_TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GL2,GLBuffer,GLTexture> terrain){
		
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//pass-specific shader uniforms
		shaderTerrain.setLightAndFog(gl, light, fogDensity);
		shaderTerrain.setMaterialLightingCoefficients(gl, terrain.getMaterial().getMaterial1());
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().bind(gl, ShaderTerrainLightPass.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().bind(gl, ShaderTerrainLightPass.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().bind(gl, ShaderTerrainLightPass.COLOR2_TEXTURE_UNIT);
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<GL2,GLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		
		if(CHECK_VALIDATION){ shaderTerrain.checkValidateStatus(gl); } //[TODO]
		gl.glDrawElements(GL2ES2.GL_TRIANGLES, 3*tileIBO.getTriCount(), GL2ES2.GL_UNSIGNED_SHORT, 0);
	}

	@Override
	public void actionTerrainEnd(Terrain<GL2,GLBuffer,GLTexture> terrain) {
		
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().unbind(gl, ShaderTerrainLightPass.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().unbind(gl, ShaderTerrainLightPass.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().unbind(gl, ShaderTerrainLightPass.COLOR2_TEXTURE_UNIT);
	}
	
	protected boolean bindTexColor(GLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindTexBump(GLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexBump){ return false; }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindShader(){
		//bind shader if it's not already, first unbinding previous shader if there is one
		//returns false if it did not bind anything
		
		if(lastActiveObjectShader == shader){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shader.useShader(gl);
		shader.setViewMatrices(gl, camera);
		shader.setLightAndFog(gl, light, fogDensity);
		
		lastActiveObjectShader = shader;
		return true;
	}
	
	protected boolean bindShaderBump(){
		//bind shader if it's not already, first unbinding previous shader if there is one
		//returns false if it did not bind anything
		
		if(lastActiveObjectShader == shaderBump){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderBump.useShader(gl);
		shaderBump.setViewMatrices(gl, camera);
		shaderBump.setLightAndFog(gl, light, fogDensity);
		
		lastActiveObjectShader = shaderBump;
		return true;
	}
	
	protected void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT); }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		light = null;
		camera = null;
	}
}
