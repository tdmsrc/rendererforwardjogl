package engine.renderer.forwardjogl.core;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;

import engine.renderer.DrawableIBO;
import engine.renderer.forwardjogl.object.GLBuffer;
import engine.renderer.forwardjogl.object.GLTexture;
import engine.renderer.forwardjogl.shader.ShaderProgram;
import engine.renderer.forwardjogl.shader.lightpass.ShaderZPrepass;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainGeneric;
import engine.renderer.forwardjogl.shader.terrain.ShaderTerrainZPrepass;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;


public class RenderPassZPrepass 
	extends SceneTraversal<GL2,GLBuffer,GLTexture>{

	private ShaderZPrepass shaderAlpha;
	private ShaderZPrepass shaderNoAlpha;
	private ShaderTerrainZPrepass shaderTerrain;
	
	//transient data for a single pass
	protected GL2ES2 gl;
	protected Camera camera;
	protected float fogDensity;
	protected Vector3d fogColor;
	protected boolean translucentPass;
	
	
	//things that can change; store last so not flipped every object
	protected GLTexture lastActiveTexColor;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassZPrepass(GL2ES2 gl){
		shaderAlpha = new ShaderZPrepass(gl, true);
		shaderNoAlpha = new ShaderZPrepass(gl, false);
		shaderTerrain = new ShaderTerrainZPrepass(gl);
	}
	
	public void initializePass(GL2ES2 gl, Camera camera, float fogDensity, Vector3d fogColor, boolean translucentPass){
		this.gl = gl;
		this.camera = camera;
		this.fogDensity = fogDensity;
		this.fogColor = fogColor;
		this.translucentPass = translucentPass;
		
		//no active texture
		lastActiveTexColor = null;
		lastActiveObjectShader = null;
		
		setupZPrepass();
	}
	
	private void setupZPrepass(){
		
		gl.glEnable(GL2ES2.GL_DEPTH_TEST);
		gl.glDepthFunc(GL2ES2.GL_LESS);
		
		gl.glDepthMask(true);
		gl.glColorMask(true, true, true, true);
		
		gl.glDisable(GL2ES2.GL_BLEND);
		
		//gl.glEnable(GL2ES2.GL_CULL_FACE);
		//gl.glCullFace(GL2ES2.GL_BACK);
	}

	@Override
	public void actionObject(SceneObject<GL2,GLBuffer,GLTexture> sceneObject){
		
		if(translucentPass || sceneObject.getMaterial().hasAlphaMask()){
			bindShaderAlpha();
			bindTexColor(sceneObject.getMaterial().getTexColor());
			
			shaderAlpha.setOpacity(gl, sceneObject.getDrawOptions().getOpacity());
			shaderAlpha.setObjectTransformation(gl, sceneObject);
			shaderAlpha.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindShaderNoAlpha();
			
			shaderNoAlpha.setObjectTransformation(gl, sceneObject);
			shaderNoAlpha.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}

		gl.glDrawArrays(GL2ES2.GL_TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GL2,GLBuffer,GLTexture> terrain){
		
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//pass-specific shader uniforms
		shaderTerrain.setOpacity(gl, terrain.getDrawOptions().getOpacity());
		shaderTerrain.setFog(gl, fogColor, fogDensity);
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<GL2,GLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		
		gl.glDrawElements(GL2ES2.GL_TRIANGLES, 3*tileIBO.getTriCount(), GL2ES2.GL_UNSIGNED_SHORT, 0);
	}

	@Override
	public void actionTerrainEnd(Terrain<GL2,GLBuffer,GLTexture> terrain) {
		
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
	}
	
	protected boolean bindTexColor(GLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderZPrepass.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderZPrepass.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindShaderAlpha(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderAlpha){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderAlpha.useShader(gl);
		shaderAlpha.setViewMatrices(gl, camera);
		
		shaderAlpha.setFog(gl, fogColor, fogDensity);
		
		lastActiveObjectShader = shaderAlpha;
		return true;
	}
	
	protected boolean bindShaderNoAlpha(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderNoAlpha){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderNoAlpha.useShader(gl);
		shaderNoAlpha.setViewMatrices(gl, camera);
		
		shaderNoAlpha.setFog(gl, fogColor, fogDensity);
		
		lastActiveObjectShader = shaderNoAlpha;
		return true;
	}
	
	public void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderZPrepass.COLOR_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
		fogColor = null;
	}
}
